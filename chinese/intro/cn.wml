#use wml::debian::template title="不同語言的 Debian [CN:网站:][HKTW:站台:]" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="6f2dd223c8dc504e001d0a01f1a665a5423e0213"

# $Id$
# Translator: Franklin <franklin@goodhorse.idv.tw>, Mon Nov 18 13:35:58 CST 2002

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">内容协商</a></li>
    <li><a href="#howtoset">怎樣在瀏覽器裡[CN:設置:][HKTW:設定:]語言</a></li>
    <li><a href="#override">如何覆盖语言[CN:設置:][HKTW:設定:]</a></li>
    <li><a href="#fix">故障排除</a></li>
  </ul>
</div>

<h2><a id="intro">内容协商</a></h2>

<p>
一组<a href="../devel/website/translating">翻译人员</a>\
将 Debian 网站翻译成越来越多种不同的语言。
但浏览器是怎么切换语言的？一个
叫做<a href="$(HOME)/devel/website/content_negotiation">内容协商
（content negotiation）</a>的标准
允许用户为网络内容设置他们的语言偏好。他们看到的版本是
浏览器和服务器协商出来的：浏览器将语言偏好发送给服务器，然后
服务器决定发送哪个版本（基于用户的偏好和可用的版本）。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">前往 W3C 了解更多</a></button></p>

<p>
不是所有人都了解内容协商，所以 Debian 网站的每个页面底部都有指向其他
语言版本的链接。请注意在该列表中选择一个不同的语言仅会影响当前页面，
这并不会改变您浏览器的默认语言设置。如果您点击一个链接前往不同的页面，
页面将重新以默认语言显示。
</p>

<p>
要改变您的默认语言，您有两种选择：
</p>

<ul>
  <li><a href="#howtoset">配置您的浏览器</a></li>
  <li><a href="#override">覆盖您浏览器的语言偏好</a></li>
</ul>

<p>
直接前往以下浏览器的设置步骤：
</p><toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 网站的源语言是英语。所以，将英语（<code>en</code>）添加到您语言列表的底部是一个好主意。这样，当一个页面尚未被翻译为您所偏好的任何语言时，英语将成为后备方案。</p>
</aside>

<h2><a id="howtoset">怎樣在瀏覽器裡[CN:設置:][HKTW:設定:]語言</a></h2>

<p>
在描述如何为不同的浏览器调整语言设置之前，我们有一些一般性的建议。
首先，在您偏好的语言列表中加入所有您会的语言是一个好主意。
例如，如果您的母語是中文，您可以將您的第一語言[CN:設置:][HKTW:設定:]成
中文（<code>zh</code>），然後是英文（<code>en</code>）。
</p>

<p>
其次，有些浏览器支持直接输入语言代码，而不是从菜单中选择。
如果是这种情况，请您注意，输入一个类似 <code>zh, en</code> 的
列表并不能准确地定义您偏好的语言的顺序。相反地，这只会定义
一组优先级相同的语言，服务器可以决定忽略顺序，而仅选择其中一个语言。
如果您希望真正地指定语言的优先级，您需要使用所谓的<q>quality value</q>，
它们是 0 到 1 之间的浮点数。数字越大，优先级越高。让我们回到中文和英文的例子，
您可以将上面的例子修改成这样：
</p>

<pre>
zh; q=1.0, en; q=0.5
</pre>

<h3>小心国家/地区代码</h3>

<p>
一个收到了语言偏好为 <code>zh-CN, en</code> 的[CN:文檔:][HKTW:說明文件:]请求
的服务器<strong>不一定</strong>会优先选择中文版本。仅当存在语言扩展名
为 <code>zh-cn</code> 的页面时，服务器才会选择中文版本。不过反过来则成立：
如果语言偏好列表只有 <code>zh</code>，服务器可以返回 <code>zh-cn</code> 页面。
</p>

<p>
所以，我们不建议添加两个字母的国家/地区代码，
例如 <code>zh-CN</code> 或者 <code>zh-TW</code>，除非您有特别好的理由。
如果您确实需要添加，请确保同时添加一个不含国家/地区代码的
语言代码：<code>zh-CN, zh, en</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">更多关于内容协商的内容</a></button></p>

<h3>不同浏览器的设置步骤</h3>

<p>
我们收集了一个流行的浏览器的列表，以及如何在它们的设置中改变网络
内容的语言偏好：
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  在右上角，打开菜单并点击 <em>设置</em> -&gt; <em>高级</em> -&gt; <em>语言</em>。打开 <em>语言</em> 菜单，可以看到一个语言列表。点击项目旁边的三个点来改变顺序。如有必要，您也可以添加新的语言。</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  在 <em>设置</em> -&gt; <em>语言</em> 中改变默认语言的同时，也会修改向网站请求的语言。您可以通过在 <em>设置</em> -&gt; <em>选项管理器</em> -&gt; <em>协议</em> -&gt; <em>HTTP</em>里调整 <em>Accept-Language 头</em> 来改变这一行为。</li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  在主菜单中打开 <em>首选项</em> 并切换到 <em>语言</em> 标签页。您可以在此添加、删除和排序语言。</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  在顶部的菜单栏打开 <em>设置</em>。在 <em>常规</em> 栏目中向下滚动到 <em>语言与外观</em> -&gt; <em>语言</em>。点击 <em>选择</em> 按钮来设置显示的网站的语言偏好。在同一个对话框中您也可以添加、删除和排序语言。</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  打开 <em>首选项</em> -&gt; <em>设置</em> -&gt; <em>网络</em>。<em>接受的语言</em> 可能会是 *，这是默认设置。点击 <em>区域设置</em> 按钮，您应当可以添加偏好的语言。如果不行，您可以手动输入。</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>编辑</em> -&gt; <em>首选项</em> -&gt; <em>浏览器</em> -&gt; <em>字体，语言</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>编辑</em> -&gt; <em>首选项</em> -&gt; <em>内容</em> -&gt; <em>语言</em> -&gt; <em>选择</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  点击 <em>工具</em> 图标，选择 <em>Internet 选项</em>，切换到 <em>常规</em> 标签页，点击 <em>语言</em> 按钮。点击 <em>设置语言首选项</em>，您可以在接下来的对话框中添加、删除和排序语言。</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        编辑 <em>~/.kde/share/config/kio_httprc</em> 文件，添加如下一行：<br>
        <code>Languages=zh;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        编辑 <em>~/.lynxrc</em> ，添加如下一行：<br>
        <code>preferred_language=zh; q=1.0, en; q=0.5</code><br>
        或者，您也可以按 [O] 打开浏览器设置。向下滚动到 <em>偏好的语言</em> 并添加以上内容。</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>设置和更多</em>  -&gt; <em>设置</em> -&gt; <em>语言</em> -&gt; <em>添加语言</em><br>
        点击语言项目旁边的三点按钮查看更多选项和改变顺序。</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>设置</em> -&gt; <em>浏览器</em> -&gt; <em>语言</em> -&gt; <em>语言偏好</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari 使用 macOS 和 iOS 系统的语言设置，所以要改变语言偏好，请打开 <em>系统偏好设置</em>（macOS）或 <em>设置</em>（iOS）。</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        按 [O] 打开 <em>选项设置栏</em>，向下滚动到 <em>网络设置</em> -&gt; <em>接受的语言（Accept-Language）报头</em>。按 [Enter] 改变设置（例如 <code>zh; q=1.0, en; q=0.5</code>）并按 [Enter] 确认。向下滚动并按 [OK] 保存设定。</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        点击 <em>设置</em> -&gt; <em>常规</em> -&gt; <em>语言</em> -&gt; <em>接受的语言</em>，点击 <em>添加语言</em> 并从菜单中选择。使用箭头改变语言的顺序。</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 虽然在浏览器设置中选择偏好的语言永远是更合适的，您也可以使用 cookie 覆盖设置。</p>
</aside>

<h2><a id="override">如何覆盖语言[CN:設置:][HKTW:設定:]</a></h2>

<p>
不论出于何种原因，如果您无法在您的浏览器、设备或计算环境中设置首选语言，
作为最后的手段，您也可以使用 cookie 覆盖语言设置。点击以下按钮中的一个，以将
一个语言置于语言列表的顶部。
</p>

<p>
请注意，这将设置一个 <a href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>，\
其中包含您的语言选择项。如果您一个月内没有访问该网站，您的浏览器将删除该 cookie。\
当然，您随时可以在浏览器中手动删除该 cookie，或者您可以点击 <em>Browser default</em> 按钮
立即删除该 cookie。
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">故障排除</a></h2>

<p>
有时候，虽然经过了各种努力，Debian 网站还是显示了错误的语言。
我们的第一个建议是在浏览器中清除本地[CN:緩存:][HKTW:快取:]（包括磁盘和内存[CN:緩存:][HKTW:快取:]），
然后重新加载页面。如果您完全肯定您
已经<a href="#howtoset">设置好了您的浏览器</a>，那么损坏的或者
错误配置的[CN:緩存:][HKTW:快取:]可能是问题的源头。近些年这个问题变得尤其严重，
因為越來越多
的[CN:網絡:][HKTW:網路:][CN:服務供應商:][HKTW:服務提供者:]（ISP）認為
使用[CN:緩存:][HKTW:快取:]可以減少[CN:網絡:][HKTW:網路:]的流量。
阅读关于代理服务器的<a href="#cache">章节</a>，即使您
认为您没有使用代理服务器。
</p>

<p>
当然，也确实有可能就是 <a href="https://www.debian.org/">www.debian.org</a> 的問題。
虽然，在這幾年來我們所接收到的語言錯誤的報告中，只有極少數真的是我們的問題，但这
确实是可能发生的。所以
我們建議您在跟我們聯絡之前，先检查自己的设置问题和[CN:緩存:][HKTW:快取:]问题。
如果您發現 <a
href="https://www.debian.org/">https://www.debian.org/</a> 可以運作，但是有一個[CN:鏡像站點
:][HKTW:映射網站:]不行，請您向我們報告，我們會與映射[CN:站點:][HKTW:站台:]的維
護者聯繫。
</p>

<h3><a name="cache">代理[CN:服務器:][HKTW:伺服器:]的潛在問題</a></h3>

<p>
代理（proxy）[CN:服務器:][HKTW:伺服器:]基本上是一個沒有自己內容的網站[CN:服
務器:][HKTW:伺服器:]。它們介于[CN:用戶:][HKTW:使用者:]和真正的[CN:網絡服務器
:][HKTW:網路伺服器:]之間。它們抓取您所需要的網頁，然後將網頁轉給您，但同時保存
一個拷貝作為[CN:緩存:][HKTW:快取:]，以便以後取用。這可以讓很多[CN:用戶:][HKTW:
使用者:]要看同一個網頁時，[CN:網絡:][HKTW:網路:]帶寬的壓力大大減少。
</p>

<p>
大多數時候，這是個好主意，但是如果[CN:緩存:][HKTW:快取:]出問題的話，這個辦
法就不行了。特別是有些較舊的代理[CN:服務器:][HKTW:伺服器:]不支持內容協商
[CN:机:][HKTW:機:]制時。這會導致它們只存放其中一種語言的網頁[CN:緩存
:][HKTW:快取:]，而不會理會後續即使是不同語言的要求。唯一的解決方式就是更新或改
換[CN:緩存:][HKTW:快取:][CN:軟件:][HKTW:軟體:]。
</p>

<p>
在過去，人們只在他們的瀏覽器被自己[CN:設置:][HKTW:設定:]過後才能使用一個代理
。但是現在就不是這樣了。您的[CN:網絡服務供應商:][HKTW:網路服務提供者:]（ISP）可能會強
迫將所有的 HTTP [CN:连接:][HKTW:連線:][CN:请求:][HKTW:要求:][CN:重定向:][HKTW:導向:]到一個透明的代理[CN:服務器
:][HKTW:伺服器:]。如果這個代理[CN:服務器:][HKTW:伺服器:]無法正確處理內容協商，
[CN:用戶:][HKTW:使用者:]可能就會接收到錯誤語言的[CN:緩存:][HKTW:快取:]網頁。這
時唯一的辦法就是向您的 ISP 提意見，要求他們[CN:升級
:][HKTW:改善:]或更新他們的[CN:緩存軟件:][HKTW:快取軟體:]。
</p>
