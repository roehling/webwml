#use wml::debian::translation-check translation="fe8a2087097d88ffc91293a2c8530e551b714fce" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Samba, en SMB/CIFS-fil-, print- og 
loginserver til Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2124">CVE-2016-2124</a>

    <p>Stefan Metzmacher rapporterede at SMB1-klientforbindelse kunne 
    nedgraderes til ren tekst-autentifikation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">CVE-2020-25717</a>

    <p>Andrew Bartlett rapporterede at Samba kunne mappe domænebrugere til 
    lokale brugere på en uønsket måde, hvilket muliggjorde rettighedsforøgelse. 
    Opdateringen indfører et nyt parameter <q>min domain uid</q> (som standard 
    sat til 1000), for ikke at acceptere en UNIX-uid under denne værdi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25718">CVE-2020-25718</a>

    <p>Andrew Bartlett rapporterede at Samba som AD DC, når kombineret med en 
    RODC, ikke bekræftede hvorvidt RODC'en havde tilladelse til at udskrive en 
    ticket for den bruger, hvilket gjorde det muligt for en RODC at udskrive 
    administratortickets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25719">CVE-2020-25719</a>

    <p>Andrew Bartlett rapporterede at Samba som AD DC, ikke altid stolede på 
    SID'en og PAC i Kerberos-tickets, og kunne blive forvirret vedrørende den 
    bruger, en ticket repræsenterer.  Hvis en priviligeret konto blev angrebet, 
    kunne det føre til en total domænekompromittering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25721">CVE-2020-25721</a>

    <p>Andrew Bartlett rapporterede at Samba som AD DC, ikke stillede en måde 
    til rådighed for Linux-applikationer til at få adgang til en pålidelig SID 
    (og samAccountName) i ustedte tickets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">CVE-2020-25722</a>

    <p>Andrew Bartlett rapporterede at Samba som AD DC, ikke foretog 
    tilstrækkelig adgangs- og overensstemmelseskontrol af opbevarede data, som 
    potentielt muliggjorde en totalt domænekompromittering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3738">CVE-2021-3738</a>

    <p>William Ross rapporterede at Samba AD DC's RPC-server kunne anvende 
    hukommelse, som var frigivet når en under-forbindelse var lukket, medførende 
    lammelsesangreb og potentielt rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23192">CVE-2021-23192</a>

    <p>Stefan Metzmacher rapporterede at hvis en klient til en Samba-server 
    sendte en meget stor DCE/RPC-forespørgsel, og valgte at fragmentere den, 
    kunne en angriber erstatte senere fragementer med deres egne data, og dermed 
    omgå signaturkrav.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 2:4.13.13+dfsg-1~deb11u2.</p>

<p>Vi anbefaler at du opgraderer dine samba-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende samba, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5003.data"
