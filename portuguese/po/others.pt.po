# Brazilian Portuguese translation for Debian website others.pot
# Copyright (C) 2003-2015 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-02-02 21:26-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Canto dos(as) Novos(as) Membros(as)"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Passo 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Passo 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Passo 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Passo 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Passo 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Passo 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Passo 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Lista de itens para candidatos(as)"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Veja <a href=\"m4_HOME/intl/french/\"> https://www.debian.org/intl/french/</"
"a> (disponível somente em francês) para maiores informações."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Mais informações"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Veja <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (disponível somente em espanhol) para maiores informações."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefone"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Endereço"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produtos"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Camisetas"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "bonés"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "adesivos"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "canecas"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "outras roupas"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "camiseta polo"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbees"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "mousepads"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "crachás"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "cestas de basquete"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "brincos"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "maletas"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "guarda-chuvas"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "fronhas"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "chaveiros"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Canivetes suíços"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "Pendrives USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "cordões"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "outros"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Idiomas disponíveis:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Entrega internacional:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "na Europa"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "País de origem:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Doa dinheiro ao Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "O dinheiro é usado para organizar eventos locais de software livre"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Com&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Sem&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "PostScript Encapsulado"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini botão)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "mesmo que o acima"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Há quanto tempo você usa Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Você é uma desenvolvedora Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Em quais áreas do Debian você está envolvida?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "O que te fez se interessar em trabalhar com o Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Você tem algumas dicas para mulheres interessadas em se envolver mais com o "
"Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Você está envolvida em outros grupos de mulheres na tecnologia? Quais?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Um pouco mais sobre você..."

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<total_consultant> consultores Debian listados em <total_country> países. "

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "Desconhecido"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "ALL"
#~ msgstr "TUDO"

#~ msgid "Architecture:"
#~ msgstr "Arquitetura:"

#~ msgid "BAD"
#~ msgstr "RUIM"

#~ msgid "BAD?"
#~ msgstr "RUIM?"

#~ msgid "Booting"
#~ msgstr "Inicializando"

#~ msgid "Building"
#~ msgstr "Construindo"

#~ msgid "Clear"
#~ msgstr "Limpar"

#~ msgid "Company:"
#~ msgstr "Companhia"

#~ msgid "Download"
#~ msgstr "Download"

#~ msgid "Email:"
#~ msgstr "E-mail:"

#~ msgid "Location:"
#~ msgstr "Localização:"

#~ msgid "Mailing List Subscription"
#~ msgstr "Inscrição em Listas de Discussão"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Desinscrição das listas de discussão"

#~ msgid "Moderated:"
#~ msgstr "Moderada:"

#~ msgid "Name:"
#~ msgstr "Nome:"

#~ msgid "No description given"
#~ msgstr "Nenhuma descrição dada"

#~ msgid "No images"
#~ msgstr "Sem imagens"

#~ msgid "No kernel"
#~ msgstr "Sem kernel"

#~ msgid "Not yet"
#~ msgstr "Ainda Não"

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Note que a maioria das listas de discussão Debian são fóruns públicos. "
#~ "Quaisquer mensagens enviadas para elas serão publicadas nos arquivos "
#~ "públicos das listas de discussão e indexados por mecanismos de busca. "
#~ "Você só deveria se inscrever numa lista de discussão Debian usando um "
#~ "endereço de e-mail que você não se importa em tornar público."

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "Old banner ads"
#~ msgstr "Banners antigos"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Somente mensagens assinadas por um desenvolvedor Debian serão aceitas "
#~ "nesta lista."

#~ msgid "Package"
#~ msgstr "Pacote"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Por favor, respeite a <a href=\"./#ads\">política de propaganda das "
#~ "listas de discussão Debian</a>."

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Por favor, selecione as listas nas quais deseja inscrever-se:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr ""
#~ "Por favor, selecione as listas das quais você deseja desinscrever-se:"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Postagem de mensagens permitida apenas para pessoas inscritas."

#~ msgid "Previous Talks:"
#~ msgstr "Palestras Anteriores:"

#~ msgid "Rates:"
#~ msgstr "Preço:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Consulte a página das <a href=\"./#subunsub\">listas de discussão</a> "
#~ "para obter informações sobre como inscrever-se usando o e-mail. Um <a "
#~ "href=\"unsubscribe\">formulário de desinscrição via web</a> também está "
#~ "disponível. "

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Consulte a página das <a href=\"./#subunsub\">listas de discussão</a> "
#~ "para obter informações sobre como desinscrever-se usando o e-mail. Um <a "
#~ "href=\"subscribe\">formulário para inscrição via web</a> também está "
#~ "disponível. "

#~ msgid "Specifications:"
#~ msgstr "Especificação:"

#~ msgid "Status"
#~ msgstr "Estado"

#~ msgid "Subscribe"
#~ msgstr "Inscrição"

#~ msgid "Subscription:"
#~ msgstr "Inscrição:"

#~ msgid "Topics:"
#~ msgstr "Tópicos:"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "Unavailable"
#~ msgstr "Indisponível"

#~ msgid "Unknown"
#~ msgstr "Desconhecido"

#~ msgid "Unsubscribe"
#~ msgstr "Desinscrever"

#~ msgid "Version"
#~ msgstr "Versão"

#~ msgid "Wanted:"
#~ msgstr "Desejado:"

#~ msgid "Where:"
#~ msgstr "Onde:"

#~ msgid "Who:"
#~ msgstr "Quem:"

#~ msgid "Willing to Relocate"
#~ msgstr "Aguardando por Realocação"

#~ msgid "Working"
#~ msgstr "Funcionando"

#~ msgid "Your E-Mail address:"
#~ msgstr "Seu endereço de e-mail:"

#~ msgid "beta 4"
#~ msgstr "beta 4"

#~ msgid "closed"
#~ msgstr "fechada"

#~ msgid "is a read-only, digestified version."
#~ msgstr "somente leitura, versão resumida (digest)"

#~ msgid "open"
#~ msgstr "aberta"

#~ msgid "or"
#~ msgstr "ou"

#~ msgid "p<get-var page />"
#~ msgstr "p<get-var page />"

#~ msgid "rc1"
#~ msgstr "rc1"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (quebrado)"
