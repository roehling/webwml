#use wml::debian::template title="Debian &ldquo;bullseye&rdquo;-utgivelsesinformasjon"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Hans F. Nordhaug <hansfn@gmail.com>

<p>
  Debian <current_release_bullseye> ble utgitt <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>. 
  <ifneq "11.0" "<current_release>"
  "Debian 11.0 ble først utgitt <:=spokendate('XXXXXXXX'):>."
  />
  Utgivelsen inkluderte mange store endringer beskrevet i vår
  <a href="$(HOME)/News/2021/20210814">pressemelding</a> og i
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

<p><strong>Debian 11 er avløst av
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
# Sikkerhetsoppdateringer ble avsluttet <:=spokendate(''xxxx-xx-xx):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Men, bullseye drar nytte av Long Term Support (LTS) til
#slutten av xxxx 20XX. LTS er begrenset til i386, amd64, armel, armhf og arm64. 
#Alle andre arkitekturer er ikke lenger støttet i bullseye.
#For mer informasjon, 
#se <a href="https://wiki.debian.org/LTS">LTS-delen av Debian-wikien</a>.
#</strong></p>

<p>
  For å få tak og installere Debian, se siden med
  <a href="debian-installer/">installasjonsinformasjon</a> og 
  <a href="installmanual">installasjonshåndboken</a>. 
  For å oppgradere fra eldre Debian utgaver, se instruksjonene i 
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

### Activate the following when LTS period starts.
#<p>Følgende datamaskinarkitekturer er støttet i LTS-perioden:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Følgende datamaskinarkitekturer er støttet i denne utgaven:</p>
# <p>Da bullseye ble utgitt var følgende datamaskinarkitekturer støttet:</p> ### Use this line when LTS starts.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgaven selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problemer</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
