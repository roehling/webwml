<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Emmet Leahy reported that libphp-adodb, a PHP database abstraction layer
library, allows to inject values into a PostgreSQL connection string.
Depending on how the library is used this flaw can result in
authentication bypass, reveal a server IP address or have other
unspecified impact.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 5.20.14-1+deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 5.20.19-1+deb11u1.</p>

<p>We recommend that you upgrade your libphp-adodb packages.</p>

<p>For the detailed security status of libphp-adodb please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libphp-adodb">\
https://security-tracker.debian.org/tracker/libphp-adodb</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5101.data"
# $Id: $
