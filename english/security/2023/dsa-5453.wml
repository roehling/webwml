<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

    <p>It was discovered that a flaw in the handling of the RPL protocol
    may allow an unauthenticated remote attacker to cause a denial of
    service if RPL is enabled (not by default in Debian).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31248">CVE-2023-31248</a>

    <p>Mingi Cho discovered a use-after-free flaw in the Netfilter
    nf_tables implementation when using nft_chain_lookup_byid, which may
    result in local privilege escalation for a user with the
    CAP_NET_ADMIN capability in any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35001">CVE-2023-35001</a>

    <p>Tanguy DUBROCA discovered an out-of-bounds reads and write flaw in
    the Netfilter nf_tables implementation when processing an
    nft_byteorder expression, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any user
    or network namespace.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 5.10.179-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5453.data"
# $Id: $
