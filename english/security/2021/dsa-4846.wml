<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16044">CVE-2020-16044</a>

    <p>Ned Williamson discovered a use-after-free issue in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21117">CVE-2021-21117</a>

    <p>Rory McNamara discovered a policy enforcement issue in Cryptohome.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21118">CVE-2021-21118</a>

    <p>Tyler Nighswander discovered a data validation issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21119">CVE-2021-21119</a>

    <p>A use-after-free issue was discovered in media handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21120">CVE-2021-21120</a>

    <p>Nan Wang and Guang Gong discovered a use-after-free issue in the WebSQL
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21121">CVE-2021-21121</a>

    <p>Leecraso and Guang Gong discovered a use-after-free issue in the Omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21122">CVE-2021-21122</a>

    <p>Renata Hodovan discovered a use-after-free issue in Blink/WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21123">CVE-2021-21123</a>

    <p>Maciej Pulikowski discovered a data validation issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21124">CVE-2021-21124</a>

    <p>Chaoyang Ding discovered a use-after-free issue in the speech recognizer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21125">CVE-2021-21125</a>

    <p>Ron Masas discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21126">CVE-2021-21126</a>

    <p>David Erceg discovered a policy enforcement issue in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21127">CVE-2021-21127</a>

    <p>Jasminder Pal Singh discovered a policy enforcement issue in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21128">CVE-2021-21128</a>

    <p>Liang Dong discovered a buffer overflow issue in Blink/WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21129">CVE-2021-21129</a>

    <p>Maciej Pulikowski discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21130">CVE-2021-21130</a>

    <p>Maciej Pulikowski discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21131">CVE-2021-21131</a>

    <p>Maciej Pulikowski discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21132">CVE-2021-21132</a>

    <p>David Erceg discovered an implementation error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21133">CVE-2021-21133</a>

    <p>wester0x01 discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21134">CVE-2021-21134</a>

    <p>wester0x01 discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21135">CVE-2021-21135</a>

    <p>ndevtk discovered an implementation error in the Performance API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21136">CVE-2021-21136</a>

    <p>Shiv Sahni, Movnavinothan V, and Imdad Mohammed discovered a policy
    enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21137">CVE-2021-21137</a>

    <p>bobbybear discovered an implementation error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21138">CVE-2021-21138</a>

    <p>Weipeng Jiang discovered a use-after-free issue in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21139">CVE-2021-21139</a>

    <p>Jun Kokatsu discovered an implementation error in the iframe sandbox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21140">CVE-2021-21140</a>

    <p>David Manouchehri discovered uninitialized memory in the USB
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21141">CVE-2021-21141</a>

    <p>Maciej Pulikowski discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21142">CVE-2021-21142</a>

    <p>Khalil Zhani discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21143">CVE-2021-21143</a>

    <p>Allen Parker and Alex Morgan discovered a buffer overflow issue in
    extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21144">CVE-2021-21144</a>

    <p>Leecraso and Guang Gong discovered a buffer overflow issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21145">CVE-2021-21145</a>

    <p>A use-after-free issue was discovered.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21146">CVE-2021-21146</a>

    <p>Alison Huffman and Choongwoo Han discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21147">CVE-2021-21147</a>

    <p>Roman Starkov discovered an implementation error in the skia library.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 88.0.4324.146-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4846.data"
# $Id: $
