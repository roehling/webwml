<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were fixed in imagemagick, a software suite,
used for editing and manipulating digital images.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20176">CVE-2021-20176</a>

    <p>A divide by zero was found in gem.c file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20241">CVE-2021-20241</a>

    <p>A divide by zero was found in  jp2 coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20243">CVE-2021-20243</a>

    <p>A divide by zero was found in dcm coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20244">CVE-2021-20244</a>

    <p>A divide by zero was found in fx.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20245">CVE-2021-20245</a>

    <p>A divide by zero was found in webp coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20246">CVE-2021-20246</a>

    <p>A divide by zero was found in resample.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20309">CVE-2021-20309</a>

    <p>A divide by zero was found in WaveImage.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20312">CVE-2021-20312</a>

    <p>An integer overflow was found in WriteTHUMBNAILImage()
    of coders/thumbnail.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20313">CVE-2021-20313</a>

    <p>A potential cipher leak was found when the calculate
    signatures in TransformSignature().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39212">CVE-2021-39212</a>

    <p>A policy bypass was found for postscript files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28463">CVE-2022-28463</a>

    <p>A bufer overflow was found in  buffer overflow in cin coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32545">CVE-2022-32545</a>

    <p>A undefined behavior (conversion outside the range of
    representable values of type <q>unsigned char</q>) was found in psd
    file handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32546">CVE-2022-32546</a>

    <p>A undefined behavior (conversion outside the range of
    representable values of type <q>long</q>) was found in pcl
    file handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32547">CVE-2022-32547</a>

    <p>An unaligned access was found in property.c</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
8:6.9.10.23+dfsg-2.1+deb10u5.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3429.data"
# $Id: $
