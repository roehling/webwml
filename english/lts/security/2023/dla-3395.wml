<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the Go programming
language. An attacker could trigger a denial-of-service (DoS), invalid
cryptographic computation, information leak, or arbitrary code
execution on the developer's computer in specific situations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28367">CVE-2020-28367</a>

    <p>Code injection in the go command with cgo allows arbitrary code
    execution at build time via malicious gcc flags specified via a
    #cgo directive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>

    <p>In archive/zip, a crafted file count (in an archive's header) can
    cause a NewReader or OpenReader panic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36221">CVE-2021-36221</a>

    <p>Go has a race condition that can lead to a net/http/httputil
    ReverseProxy panic upon an ErrAbortHandler abort.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38297">CVE-2021-38297</a>

    <p>Go has a Buffer Overflow via large arguments in a function
    invocation from a WASM module, when GOARCH=wasm GOOS=js is used.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39293">CVE-2021-39293</a>

    <p>This issue exists because of an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41771">CVE-2021-41771</a>

    <p>ImportedSymbols in debug/macho (for Open or OpenFat) Accesses a
    Memory Location After the End of a Buffer, aka an out-of-bounds
    slice situation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44716">CVE-2021-44716</a>

    <p>net/http allows uncontrolled memory consumption in the header
    canonicalization cache via HTTP/2 requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44717">CVE-2021-44717</a>

    <p>Go on UNIX allows write operations to an unintended file or
    unintended network connection as a consequence of erroneous
    closing of file descriptor 0 after file-descriptor exhaustion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23806">CVE-2022-23806</a>

    <p>Curve.IsOnCurve in crypto/elliptic can incorrectly return true in
    situations with a big.Int value that is not a valid field element.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24921">CVE-2022-24921</a>

    <p>regexp.Compile allows stack exhaustion via a deeply nested
    expression.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.11.6-1+deb10u6.</p>

<p>We recommend that you upgrade your golang-1.11 packages.</p>

<p>For the detailed security status of golang-1.11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.11">https://security-tracker.debian.org/tracker/golang-1.11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3395.data"
# $Id: $
