<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that missing input sanitising in cups-filters, when
using the Backend Error Handler (beh) backend to create an accessible
network printer, may result in the execution of arbitrary commands.</p>


<p>For Debian 10 buster, this problem has been fixed in version
1.21.6-5+deb10u1.</p>

<p>We recommend that you upgrade your cups-filters packages.</p>

<p>For the detailed security status of cups-filters please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cups-filters">https://security-tracker.debian.org/tracker/cups-filters</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3430.data"
# $Id: $
