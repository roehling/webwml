<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Apache Traffic Server, 
a reverse and forward proxy server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47184">CVE-2022-47184</a>

    <p>The TRACE method can be used to disclose network information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30631">CVE-2023-30631</a>

    <p>Configuration option to block the PUSH method in ATS didn't work.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33933">CVE-2023-33933</a>

    <p>s3_auth plugin problem with hash calculation.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
8.1.7-0+deb10u1.</p>

<p>We recommend that you upgrade your trafficserver packages.</p>

<p>For the detailed security status of trafficserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/trafficserver">https://security-tracker.debian.org/tracker/trafficserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3475.data"
# $Id: $
