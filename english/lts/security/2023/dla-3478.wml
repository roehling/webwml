<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A memory leak has been found in yajl, a JSON parser / small validating
JSON generator written in ANSI C, which might allow an attacker to cause
an out of memory situation and potentially causing a crash.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.1.0-3+deb10u1.</p>

<p>We recommend that you upgrade your yajl packages.</p>

<p>For the detailed security status of yajl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/yajl">https://security-tracker.debian.org/tracker/yajl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3478.data"
# $Id: $
