<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the CompareTool of iText, a Java PDF library which
uses the external ghostscript software to compare PDFs at a pixel level,
allowed command injection when parsing a specially crafted filename.</p>

<p>For Debian 10 buster, this problem has been fixed in version
5.5.13-1+deb10u1.</p>

<p>We recommend that you upgrade your libitext5-java packages.</p>

<p>For the detailed security status of libitext5-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libitext5-java">https://security-tracker.debian.org/tracker/libitext5-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3273.data"
# $Id: $
