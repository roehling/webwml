<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were multiple cross-site scripting (XSS) vulnerabilities in the <tt>squid3</tt> caching proxy server</p> 

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13345">CVE-2019-13345</a>

    <p>The cachemgr.cgi web module of Squid through 4.7 has XSS via the user_name or auth parameter.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.8-6+deb8u7.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1847.data"
# $Id: $
