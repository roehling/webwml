<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in the JasPer JPEG-2000 library that could
lead to a denial-of-service (application crash), memory leaks and
potentially the execution of arbitrary code if a malformed image file
is processed.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.900.1-debian1-2.4+deb8u5.</p>

<p>We recommend that you upgrade your jasper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1628.data"
# $Id: $
