<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Apache Xalan Java XSLT library is vulnerable to an integer truncation issue
when processing malicious XSLT stylesheets. This can be used to corrupt Java
class files generated by the internal XSLTC compiler and execute arbitrary Java
bytecode. In Debian the vulnerable code is in the bcel source package.</p>

<p>For Debian 10 buster, this problem has been fixed in version
6.2-1+deb10u1.</p>

<p>We recommend that you upgrade your bcel packages.</p>

<p>For the detailed security status of bcel please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bcel">https://security-tracker.debian.org/tracker/bcel</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3155.data"
# $Id: $
