<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were four issues in ldns, a library used in
Domain Name System (DNS) related programs:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19860">CVE-2020-19860</a>

    <p>When ldns version 1.7.1 verifies a zone file, the
    ldns_rr_new_frm_str_internal function has a heap out of bounds read
    vulnerability. An attacker can leak information on the heap by constructing
    a zone file payload.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19861">CVE-2020-19861</a>

    <p>When a zone file in ldns 1.7.1 is parsed, the function
    ldns_nsec3_salt_data is too trusted for the length value obtained from the
    zone file. When the memcpy is copied, the 0xfe - ldns_rdf_size(salt_rdf)
    byte data can be copied, causing heap overflow information
    leakage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000231">CVE-2017-1000231</a>

    <p>A double-free vulnerability in parse.c in ldns 1.7.0 have unspecified
    impact and attack vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000232">CVE-2017-1000232</a>

    <p>A double-free vulnerability in str2host.c in ldns 1.7.0 have unspecified
    impact and attack vectors.</p></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these four problems have been fixed in version
1.7.0-1+deb9u1.</p>

<p>We recommend that you upgrade your ldns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2910.data"
# $Id: $
