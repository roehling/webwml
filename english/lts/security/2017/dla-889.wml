<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that potrace, an utility to transform bitmaps into
vector graphics, was affected by an integer overflow in the findnext
function, allowing remote attackers to cause a denial of service
(invalid memory access and crash) via a crafted BMP image.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.10-1+deb7u2.</p>

<p>We recommend that you upgrade your potrace packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-889.data"
# $Id: $
