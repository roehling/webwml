<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In version 4.8.0 and earlier of The Sleuth Kit (TSK), there is
a stack buffer overflow vulnerability in the YAFFS file timestamp
parsing logic in yaffsfs_istat() in fs/yaffs.c.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.1.3-4+deb8u2.</p>

<p>We recommend that you upgrade your sleuthkit packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2137.data"
# $Id: $
