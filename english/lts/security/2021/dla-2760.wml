<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in nettle, a low level
cryptographic library, which could result in denial of service (remote
crash in RSA decryption via specially crafted ciphertext, crash on ECDSA
signature verification) or incorrect verification of ECDSA signatures.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
3.3-1+deb9u1.</p>

<p>We recommend that you upgrade your nettle packages.</p>

<p>For the detailed security status of nettle please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nettle">https://security-tracker.debian.org/tracker/nettle</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2760.data"
# $Id: $
