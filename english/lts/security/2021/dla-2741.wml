<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Lukas Euler discovered a path traversal vulnerability in commons-io, a Java
library for common useful IO related classes. When invoking the method
FileNameUtils.normalize with an improper input string, like "//../foo", or
"\\..\foo", the result would be the same value, thus possibly providing access
to files in the parent directory, but not further above (thus <q>limited</q> path
traversal), if the calling code would use the result to construct a path value.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.5-1+deb9u1.</p>

<p>We recommend that you upgrade your commons-io packages.</p>

<p>For the detailed security status of commons-io please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/commons-io">https://security-tracker.debian.org/tracker/commons-io</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2741.data"
# $Id: $
