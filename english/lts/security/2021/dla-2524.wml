<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in spice-vdagent, a spice
guest agent for enchancing SPICE integeration and experience.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15108">CVE-2017-15108</a>

    <p>spice-vdagent does not properly escape save directory before
    passing to shell, allowing local attacker with access to the
    session the agent runs in to inject arbitrary commands to be
    executed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25650">CVE-2020-25650</a>

    <p>A flaw was found in the way the spice-vdagentd daemon handled file
    transfers from the host system to the virtual machine. Any
    unprivileged local guest user with access to the UNIX domain
    socket path `/run/spice-vdagentd/spice-vdagent-sock` could use
    this flaw to perform a memory denial of service for spice-vdagentd
    or even other processes in the VM system. The highest threat from
    this vulnerability is to system availability. This flaw affects
    spice-vdagent versions 0.20 and previous versions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25651">CVE-2020-25651</a>

    <p>A flaw was found in the SPICE file transfer protocol. File data
    from the host system can end up in full or in parts in the client
    connection of an illegitimate local user in the VM system. Active
    file transfers from other users could also be interrupted,
    resulting in a denial of service. The highest threat from this
    vulnerability is to data confidentiality as well as system
    availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25652">CVE-2020-25652</a>

    <p>A flaw was found in the spice-vdagentd daemon, where it did not
    properly handle client connections that can be established via the
    UNIX domain socket in `/run/spice-vdagentd/spice-vdagent-sock`.
    Any unprivileged local guest user could use this flaw to prevent
    legitimate agents from connecting to the spice-vdagentd daemon,
    resulting in a denial of service. The highest threat from this
    vulnerability is to system availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25653">CVE-2020-25653</a>

    <p>A race condition vulnerability was found in the way the
    spice-vdagentd daemon handled new client connections. This flaw
    may allow an unprivileged local guest user to become the active
    agent for spice-vdagentd, possibly resulting in a denial of
    service or information leakage from the host. The highest threat
    from this vulnerability is to data confidentiality as well as
    system availability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.17.0-1+deb9u1.</p>

<p>We recommend that you upgrade your spice-vdagent packages.</p>

<p>For the detailed security status of spice-vdagent please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spice-vdagent">https://security-tracker.debian.org/tracker/spice-vdagent</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2524.data"
# $Id: $
