<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8695">CVE-2020-8695</a>

    <p>Observable discrepancy in the RAPL interface for some
    Intel(R) Processors may allow a privileged user to
    potentially enable information disclosure via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8696">CVE-2020-8696</a>

    <p>Improper removal of sensitive information before storage
    or transfer in some Intel(R) Processors may allow an
    authenticated user to potentially enable information
    disclosure via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8698">CVE-2020-8698</a>

    <p>Improper isolation of shared resources in some
    Intel(R) Processors may allow an authenticated user to
    potentially enable information disclosure via local access.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.20201118.1~deb9u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2546.data"
# $Id: $
