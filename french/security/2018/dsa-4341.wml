#use wml::debian::translation-check translation="23827ea77e8391f791f7a8741cbbf1f0f50a6c24" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de
données MariaDB. Ces vulnérabilités ont été corrigées en mettant à niveau
MariaDB vers la nouvelles version amont 10.1.37. Veuillez lire les notes de
publication de MariaDB 10.1 pour de plus amples détails :</p>

<ul>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10127-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10127-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10128-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10128-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10129-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10129-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10130-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10130-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10131-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10131-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10132-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10132-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10133-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10133-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10134-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10134-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10135-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10135-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10136-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10136-release-notes/</a></li>
<li><a href="https://mariadb.com/kb/en/mariadb/mariadb-10137-release-notes/">https://mariadb.com/kb/en/mariadb/mariadb-10137-release-notes/</a></li>
</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 10.1.37-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mariadb-10.1.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mariadb-10.1,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mariadb-10.1">\
https://security-tracker.debian.org/tracker/mariadb-10.1</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4341.data"
# $Id: $
