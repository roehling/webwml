#use wml::debian::translation-check translation="40b46f2a1e459acfcd3e78c1da7b0c50f4d4970c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Squid, un cache de 
serveur mandataire web riche en fonctionnalités, qui pouvaient avoir pour
conséquences la divulgation d'informations sensibles dans le gestionnaire
de cache (<a href="https://security-tracker.debian.org/tracker/CVE-2022-41317">CVE-2022-41317</a>),
un déni de service ou la divulgation d'informations si Squid est configuré
pour une négociation d'authentification avec les assistants
d'authentification SSPI et SMB
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41318">CVE-2022-41318</a>).</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 4.13-10+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/squid">\
https://security-tracker.debian.org/tracker/squid</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5258.data"
# $Id: $
