#use wml::debian::template title="Programme de Sruthi Chandran" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="7c4fbb6a240c8d09a461025f9dc6518c29be077b" maintainer="Jean-Paul Guillonneau"

<h2>Qui suis-je ?</h2>
<p>Je suis la première (et la seule) femme développeuse Debian en Inde.
</p>
<p>Quelques-unes de mes activités consistent en :</p>
<ul>
	<li>entretien de paquets ruby, javascript, golang et
	de fontes (approximativement 200) depuis 2016 dont
	les plus importants sont gitlab, gitaly, rails, etc.
	<li>membre de l’équipe communauté depuis août 2020 ;
	<li>responsable de candidature depuis juillet 2020 ;
	<li>membre de l’équipe d’organisation de DebConf20 ;
	<li>participation à l’équipe Outreach depuis août/sept 2020 ;
	<li>conduite de l’équipe gagnante pour DebConf India
	 et si possible celle de DebConf India 2023(?).
</ul>
<p>j’ai parrainé des personnes pour contribuer à Debian. Je me suis
impliquée dans l’organisation de nombreux ateliers d’empaquetage et dans
d’autres évènements concernant Debian dans toute l’Inde (dont la dernière
MiniDebConf India 2021 multilingue). Je me suis impliquée aussi dans
l’organisation d’autres évènements concernant le logiciel libre tel le
<a href="https://camp.fsf.org.in/">Free Software Camp</a>.</p>
<p>Debian est à la fois ma passion et mon activité (Gitlab Inc. parraine
l’empaquetage de gitlab que je réalise).</p>

<h2>Pourquoi je me présente ?</h2>

<p>Préoccupée par le ratio hommes-femmes déséquilibré dans la communauté
du logiciel libre (et dans Debian), je m'efforce de faire toutes les
petites choses que je peux pour améliorer la situation. Combien de fois
avons-nous eu une candidature non masculine au poste de responsable du
projet ? Si je ne me trompe pas, lorsque je me suis présentée l’année
dernière, j’étais la seconde candidate au poste de DPL à ce jour. L’année
dernière, mon principal objectif était de mettre en avant les problèmes de
diversité. Je crois que j’ai réussi au moins en partie. Cette année mon principal
objectif est le même.</p>
<p>Je suis consciente que Debian réalise des choses pour accroitre la
diversité, mais comme nous pouvons le constater, cela n’est pas suffisant.
Je ne suis pas fière, mais triste, quand je dois dire que je suis la
première et unique femme développeuse Debian dans un pays aussi grand
que l’Inde. Je crois que la diversité n’est pas quelque chose à discuter
dans Debian-women ou Debian-diversity. Cela devrait venir dans la
discussion pour chaque aspect du projet.</p>

<p>L’élection du DPL est une activité importante dans notre projet et
je projette de mettre en avant le thème de la diversité à chaque élection
jusqu’à ce nous ayons un DPL non masculin.</p>

<p>Il me semble qu’une manière efficace d’encourager des personnes les plus
diverses à contribuer est d’avoir plus de visibilité sur la diversité déjà
présente dans la communauté. Plus les femmes (aussi bien cisgenres que
transgenres), les hommes trans et les personnes non-binaires
qui font déjà partie du projet deviendront davantage visibles plutôt que de
rester cachés quelque part dans le projet, plus les personnes de genre
différent se sentiront bien pour rejoindre notre communauté. La diversité
ethnique ou géographique est aussi un point important méritant notre
attention.</p>

<h2>Quels sont mes projets comme DPL</h2>
<p>Si on regarde en arrière, mon inexpérience était évidente pour mon
programme de DPL l’année dernière. Cette année, j’ai fait en sorte de
proposer un programme fort.</p>
<p>Voici quelques points importants et ce que j’ai prévu de faire.</p>

<h3>1. Diversité</h3>
<p>Puisque la diversité est ma principale préoccupation, commençons
par cela.
</p>

<h4><b>Budget Diversité</b></h4>
<p>En dépit de la dépense importante faite pour la diversité, il ne
semble pas que le résultat attendu soit là. Ma première tâche comme DPL
sera de revoir le modèle de dépense existant pour analyser pourquoi et
où nous nous trompons. Lorsque je dis que nous devons nous focaliser
sur la diversité, je ne veux pas dire que nous devons dépenser de
l’argent aveuglément au nom de la diversité. Je suis moi-même consciente
de cas où les dépenses pour la diversité ont fait du mal plutôt qu’un
bien quelconque.
</p>
<p>Une autre chose que je voudrais mettre en avant tout en respectant
le budget de diversité est de le médiatiser davantage pour que les
personnes qui le méritent le sachent et pour que nous n’arrivions
pas à le dépenser juste par souci de le faire.</p>

<h4><b>Activités Diversité</b></h4>
<p>J’ai entendu que le projet Debian-women a réalisé des choses
incroyables jusqu’à ce qu’il s’endorme. Désormais, nous avons l’initiative
Debian-diversity, mais n’avons pas encore gagné un grand dynamisme. Je
projette de rationaliser son activité et, si possible, de déléguer une
équipe se concentrant sur la diversité. Celle-ci devra coordonner toutes
les activités relatives à la diversité dans Debian. Elle sera aussi
impliquée dans les décisions et les dépenses du budget diversité.</p>

<h4><b>Activités locales</b></h4>
<p>Debian-localgroups qui a été conceptualisé par notre DPL actuel est
une très bonne idée pour promouvoir la diversité géographique. Je prévois
d’utiliser son potentiel pour organiser plus d’évènements locaux et
pour avoir des personnes plus diverses contribuant à Debian.</p>

<h3>2. Outreach</h3>
<p>Ma deuxième préoccupation concerne les activités de promotion. Je
travaille avec l’équipe Debian-outreach depuis six à sept mois. Je pense
que la promotion est une des actions importantes qui n’est pas assez
valorisée. Notre équipe outreach est devenue une coordination pour GSoC
et Outreachy. J’ai quelques idées pour outreach.</p>

<h4><b>Debian camp</b></h4>
<p>Debian camp est un concept que j’ai emprunté
au <a href="https://camp.fsf.org.in/">Free Software Camp</a> pour lequel
je faisais partie des organisateurs principaux. Fondamentalement, le
concept est un programme de parrainage en ligne de trois à quatre mois.
L’avantage d’un programme spécifique à Debian est d’adapter le programme
à nos besoins.</p>
<p>Il y aurait deux parties dans le programme. En premier lieu, la
préoccupation principale concernerait la philosophie de Debian (et du
logiciel libre) et les raisons pour lesquelles il faut contribuer
à Debian, etc. Nous pourrions avoir des sessions communes sur ces sujets
avec les stagiaires. En second lieu, les stagiaires travailleraient avec
leurs mentors sur les projets de leur choix.</p>
<p>Dans cette activité, je prévois que l’équipe Debian-outreach
travaillerait comme coordinatrice avec les équipes Debian-diversity,
Debian-localgroups, Debian Academy, etc. L’équipe Debian-outreach
réaliserait en toile de fond les activités d’organisation, de
prévision, etc., tandis que les autres équipes aideraient avec leurs
ressources en personnes, mentors, projets, etc. J’envisage
particulièrement que les Debian-localgroups soient impliqués de façon
à avoir des activités régionalisées et pas uniquement en anglais.</p>
<p>Plus de nouvelles sont disponibles dans
<a href="https://salsa.debian.org/debian-camp-team">salsa</a>.</p>
<p>La question de verser ou non des bourses pourrait être discutée et
décidée plus tard (nous n’avions pas de bourses pour le Free Software
Camp et ce fut un succès).</p>

<h4><b>DebConf Boot-camp</b></h4>
<p>C’est une rencontre physique qui pourrait se produire pendant le
Debcamp (c’est une idée que j’ai empruntée à mollydb). Je crois qu’il
n’y a rien de tel qu’un mentor présent physiquement.
</p>
<p>Souvent nous avons des nouvelles personnes qui assistent à la
DebConf et qui sont perplexes quant à la façon de contribuer. Cette
activité de démarrage peut les aider à trouver une voie. Je crois que
beaucoup seraient intéressés à nous aider de cette façon. La manière
précise de le mettre en pratique pourrait être discutée et décidée
plus tard.</p>

<h4><b>Examen de nos activités actuelles</b></h4>
<p>Un examen minutieux de nos activités de promotion est souhaitable
depuis longtemps. Nous avons besoin d’examiner l’efficacité de notre
participation à GSoC et Outreachy, que pouvons-nous faire mieux et que
pouvons-nous faire de neuf.</p>

<h3>3. Communauté</h3>
<p>La communauté Debian est une grande communauté. Mais cela ne signifie
pas que nous sommes parfaits. Quelques points que j'aimerais développer
sont :</p>
<h4><b>Accueil</b></h4>
<p>Même si nous disons que nous sommes une communauté accueillante, pour une
personne externe cela peut ne pas apparaitre ainsi. Il y a des tas de
raisons. La principale est que la communication en ligne amène à une
mauvaise communication et à des échanges virulents. En tant que communauté,
nous avons besoin d’avancer pour rendre Debian plus accueillante, pour
les nouveaux venus, comme pour les anciens. Je suis très enthousiaste
à l’idée de pouvoir faciliter les discussions en tant que DPL.
</p>

<h4><b>Acceptation du changement</b></h4>
<p>Il a été souvent observé que Debian en tant que communauté est
réticente au changement la plupart du temps. Comme DPL, je voudrais
développer une culture d’acceptation du changement. Je voudrais
encourager et faciliter la proposition d’idées nouvelles et le processus
d’amélioration de Debian.</p>

<h2>Engagement en temps</h2>

<p>
Un des avantages que j’ai sur la plupart des anciens ou actuels candidats
est le temps dont je dispose pour mes activités de DPL. Mon organisation
de travail est telle que je décide quand et pour combien de temps je
travaille. Cela signifie que je pourrais donner une grande priorité
à mes activités de DPL.</p>
