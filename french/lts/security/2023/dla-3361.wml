#use wml::debian::translation-check translation="24aa2fe804a4167fdb9b0830397d9e440ad03388" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de déni de
service à distance dans Redis, une base de données populaire clé-valeur.</p>

<p>Des utilisateurs authentifiés pouvaient utiliser une chaine correspondant à
des commandes (telles que <code>SCAN</code> ou <code>KEYS</code>) avec un motif
contrefait pour l'occasion pour déclencher une attaque par déni de service,
provoquant un plantage et une utilisation de 100 % du temps CPU.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36021">CVE-2022-36021</a>

<p>Redis est une base de données en mémoire qui persiste sur le disque. Des
utilisateurs authentifiés pouvaient utiliser une chaine correspondant à des
commandes (telles que <code>SCAN</code> ou <code>KEYS</code>) avec un motif
contrefait pour l'occasion pour déclencher une attaque par déni de service,
provoquant un plantage et une utilisation de 100 % du temps CPU. Le problème a
été corrigé dans les versions 6.0.18, 6.2.11, 7.0.9 de Redis</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5:5.0.14-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets redis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3361.data"
# $Id: $
