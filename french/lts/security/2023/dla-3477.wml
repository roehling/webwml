#use wml::debian::translation-check translation="ab34a4c04b26a28a19950f96af85c6867ac96e0f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans l’interpréteur de Python 3.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-20107">CVE-2015-20107</a>

<p>Le module mailcap n’ajoutait pas les caractères d’échappement dans les
commandes trouvées dans le fichier mailcap du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10735">CVE-2020-10735</a>

<p>Prévention de DoS avec de très grands entiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3426">CVE-2021-3426</a>

<p>Suppression de la fonctionnalité pydoc getfile qui pouvait être mal utilisée
pour lire des fichiers arbitraires sur le disque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

<p>Déni de service par expression rationnelle dans AbstractBasicAuthHandler de
la classe urllib.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

<p>Boucle infinie dans de code du client HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

<p>Faire que ftplib n’accepte pas la réponse PASV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45061">CVE-2022-45061</a>

<p>Temps quadratique dans le décodeur IDNA.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.7.3-2+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.7,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.7">\
https://security-tracker.debian.org/tracker/python3.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3477.data"
# $Id: $
