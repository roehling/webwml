#use wml::debian::translation-check translation="d2aa1ee6f88dacf488d7fdbc7f7660101004b28f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans OpenImageIO,
une bibliothèque pour lire et écrire des images. Des dépassements de tampon et
des erreurs de programmation de lecture ou d’écriture hors limites pouvaient
conduire à un déni de service (plantage d'application) ou à l’exécution de code
arbitraire si un fichier d’image mal formé était traité.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.5~dfsg0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openimageio.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openimageio,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/openimageio">\
https://security-tracker.debian.org/tracker/openimageio</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3382.data"
# $Id: $
