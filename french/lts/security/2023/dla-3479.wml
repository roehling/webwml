#use wml::debian::translation-check translation="807c38cadbcb810a711bb02d4f25017c2ecaf781" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de déni de service ont été découvertes dans
golang-yaml.v2, une bibliothèque offrant une prise en charge de YAML pour le
langage Go.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4235">CVE-2021-4235</a>

<p>À cause du <q>chasing</q> d’alias non limité, un fichier YAML contrefait
pouvait provoquer une consommation importante de ressources par le système. Lors
de l’analyse d’une entrée d’utilisateur, cela pouvait être utilisé comme vecteur
de déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3064">CVE-2022-3064</a>

<p>L’analyse de documents YAML énormes ou malveillants pouvait utiliser un
montant excessif de CPU ou de mémoire.</p>

<p>Merci à Scarlett Moore pour la préparation de cette mise à jour.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.2.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-yaml.v2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-yaml.v2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-yaml.v2">\
https://security-tracker.debian.org/tracker/golang-yaml.v2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3479.data"
# $Id: $
