#use wml::debian::translation-check translation="0e9af03d8cf68ff5ddec5d25a056c8dacce03437" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski a découvert que l'enveloppe mysqld_safe fournie par le
serveur de base de données MySQL restreignait de façon insuffisante le
chemin de chargement pour les implémentations personnalisées de malloc, ce
qui pourrait avoir pour conséquence une élévation de privilèges.</p>

<p>La vulnérabilité est corrigée en mettant MySQL à niveau vers la nouvelle
version amont 5.5.52, qui comprend d'autres changements tels que des
améliorations de performance, des corrections de bogues, de nouvelles
fonctionnalités et éventuellement des modifications incompatibles. Veuillez
consulter les notes de publication de MySQL 5.5 pour de plus amples
détails :</p>

<ul>
<li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-51.html"></li>
<li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-52.html">https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-52.html</a></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.5.52-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-624.data"
# $Id: $
