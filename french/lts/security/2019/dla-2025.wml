#use wml::debian::translation-check translation="df921eddfd0c2f41022af77ce980d0dda4c033c9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le paquet OpenSLP avait deux problèmes de sécurité connus, décrits ci-dessous.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17833">CVE-2017-17833</a>

<p>Les publications OpenSLP dans les versions 1.0.2 et 1.1.0 ont un problème de
corruption de mémoire relative au tas qui peuvent se manifester sous forme de
déni de service ou de vulnérabilité d’exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5544">CVE-2019-5544</a>

<p>OpenSLP comme utilisé dans les équipements ESXi et Horizon DaaS a un problème
d’écrasement de tas. VMware a évalué la sévérité de ce problème comme étant de
niveau grave.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.1-10+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openslp-dfsg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2025.data"
# $Id: $
