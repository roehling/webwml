#use wml::debian::translation-check translation="dfd992039cdddf2770109e84168690fba56bdda5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur
de Tomcat pour les servlets et JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30640">CVE-2021-30640</a>

<p>Une vulnérabilité dans le « JNDI Realm » de Apache Tomcat permet à un
attaquant de s’authentifier en utilisant des différences dans un nom valable
d’utilisateur ou de contourner quelques protections fournies par « LockOut
Realm ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33037">CVE-2021-33037</a>

<p>Apache Tomcat n’analysait pas correctement l’en-tête de requête d’encodage
de transfert HTTP dans certaines circonstances conduisant à la possibilité
de dissimulation de requête lors de l’utilisation d’un mandataire inverse.
Plus particulièrement : – Tomcat ignore l’en-tête d’encodage de transfert si le
client avait déclaré accepter seulement une réponse HTTP/1.0 ; – Tomcat
accepte l’encodage désigné ; — Tomcat n’assure pas que, s’il est présent,
l’encodage de blocs soit l’encodage final.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 8.5.54-0+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat8,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">\
https://security-tracker.debian.org/tracker/tomcat8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2733.data"
# $Id: $
