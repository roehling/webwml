#use wml::debian::translation-check translation="3af2a34089d81d49ffb62e5a6a1eceb69bb65815" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenLDAP, une
implémentation libre du protocole « Lightweight Directory Access Protocol »
(LDAP). Un attaquant distant non authentifié peut tirer avantage de ces défauts
pour provoquer un déni de service (plantage du démon lapd, boucles infinies) au
moyen de paquets contrefaits pour l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.4.44+dfsg-5+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openldap.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openldap, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openldap">https://security-tracker.debian.org/tracker/openldap</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2544.data"
# $Id: $
