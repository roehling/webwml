#use wml::debian::translation-check translation="06508f187deec3fb0c93765b04bf465137307531" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que Scrapy, un cadriciel pour l'extraction de données
à partir de sites web, pouvait envoyer des autorisations HTTP ainsi que des
cookies à d'autres domaines en cas de redirections, divulguant
éventuellement des identifiants d'utilisateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.0.3-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-scrapy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-scrapy,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-scrapy">\
https://security-tracker.debian.org/tracker/python-scrapy</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2950.data"
# $Id: $
