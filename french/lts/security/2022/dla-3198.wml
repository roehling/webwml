#use wml::debian::translation-check translation="670efef084627364665db1fa390d030bbf29e109" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que phpseclib, une implémentation en pur PHP de divers
algorithmes cryptographiques et arithmétiques (version 1), gère incorrectement
la vérification de la signature PKCS#1, version 1.5, RSA. Un attaquant pourrait
voir des signatures non valables acceptées en contournant le contrôle
d’authentification dans des situations particulières.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 2.0.30-2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-phpseclib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-phpseclib,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-phpseclib">\
https://security-tracker.debian.org/tracker/php-phpseclib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3198.data"
# $Id: $
