#use wml::debian::translation-check translation="917c3b551df3a4e01f4c4967825b6bdafb8ea65c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le <a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a>
n'était que partiellement corrigé dans Perl5 Database Interface (DBI). Un
attaquant pouvait déclencher la divulgation d'informations au moyen d'un
vecteur différent.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a>

<p>Les pilotes DBD::File peuvent ouvrir des fichiers à partir de
répertoires autres que ceux passés spécifiquement au moyen de l'attribut
f_dir.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10402">CVE-2014-10402</a>

<p>Les pilotes DBD::File peuvent ouvrir des fichiers à partir de
répertoires autres que ceux passés spécifiquement au moyen de l'attribut
f_dir dans le nom de source de données (DSN). REMARQUE : ce problème existe
à cause d'un correctif incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a>.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.636-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdbi-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libdbi-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libdbi-perl">\
https://security-tracker.debian.org/tracker/libdbi-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3035.data"
# $Id: $
