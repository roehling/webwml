<define-tag pagetitle>Debian 10 aktualisiert: 10.11 veröffentlicht</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Erik Pfannenstein"

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>
Das Debian-Projekt freut sich, die elfte Aktualisierung seiner 
Oldstable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Oldstable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction atftp "Pufferüberlauf behoben [CVE-2021-41054]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 10.11">
<correction btrbk "Eigenmächtige Codeausführung behoben [CVE-2021-38173]">
<correction clamav "Neue stabile Version der Originalautoren; Speicherzugriffsfehler in clamdscan behoben, der auftritt, wenn --fdpass und --multipass zusammmen mit ExcludePath verwendet werden">
<correction commons-io "Problem mit Verzeichnisüberschreitung behoben [CVE-2021-29425]">
<correction cyrus-imapd "Dienstblockade behoben [CVE-2021-33582]">
<correction debconf "Überprüfen, ob whiptail oder dialog tatsächlich verwendbar ist">
<correction debian-installer "Neukompilierung gegen buster-proposed-updates; Linux ABI auf 4.19.0-18 aktualisiert">
<correction debian-installer-netboot-images "Neukompilierung gegen buster-proposed-updates">
<correction distcc "GCC-Cross-Compiler-Links in update-distcc-symlinks korrigiert und Unterstützung für clang und CUDA (nvcc) hinzugefügt">
<correction distro-info-data "Enthaltene Daten für mehrere Veröffentlichungen aktualisiert">
<correction dwarf-fortress "Nicht verteilbare vorkompilierte dynamische Bibliotheken aus dem Quell-Tarball entfernt">
<correction espeak-ng "Zusammenspiel von espeak und mbrola-fr4 in dem Fall verbessert, dass mbrola-fr1 nicht installiert ist">
<correction gcc-mingw-w64 "Umgang mit gcov überarbeitet">
<correction gthumb "Heap-basierten Pufferüberlauf beseitigt [CVE-2019-20326]">
<correction hg-git "Test-Fehlschläge mit aktuellen git-Versionen gelöst">
<correction htslib "autopkgtest auf i386 korrigiert">
<correction http-parser "HTTP-Anfrageschmuggel unterbunden [CVE-2019-15605]">
<correction irssi "Use-after-free-Problem bei SASL-Anmeldung am Server behoben [CVE-2019-13045]">
<correction java-atk-wrapper "Auch den dbus verwenden, um festzustellen, ob die Barrierefreiheit eingeschaltet ist">
<correction krb5 "KDC-Absturz wegen Nullzeiger-Dereferenzierung bei FAST-Anfragen ohne Server-Feld behoben [CVE-2021-37750]; Speicherleck in krb5_gss_inquire_cred geflickt">
<correction libdatetime-timezone-perl "Neue stabile Version der Originalautoren; Sommerzeitregeln für Samoa und Joardanien aktualisiert; nicht stattfindende Schaltsekunde am 31.12.2021 bestätigt">
<correction libpam-tacplus "Verhindert, dass gemeinsame Geheimnisse im Klartext ins Systemprotokoll geschrieben werden [CVE-2020-13881]">
<correction linux "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q> (/proc/$pid/attr/-Öffner mm_struct nachverfolgen), damit die Probleme mit lxc-attach gelöst werden; neue stabile Veröffentlichung der Originalautoren; ABI-Version auf 18 angehoben; [rt] Aktualisiert auf 4.19.207-rt88; usb: hso: Fehlerbehandlungscode von hso_create_net_device überarbeitet [CVE-2021-37159]">
<correction linux-latest "Aktualisierung auf Kernel-ABI 4.19.0-18">
<correction linux-signed-amd64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q> (/proc/$pid/attr/-Öffner mm_struct nachverfolgen), damit die Probleme mit lxc-attach gelöst werden; neue stabile Veröffentlichung der Originalautoren; ABI-Version auf 18 angehoben; [rt] Aktualisierung auf 4.19.207-rt88; usb: hso: Fehlerbehandlungscode von hso_create_net_device überarbeitet [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q> (/proc/$pid/attr/-Öffner mm_struct nachverfolgen), damit die Probleme mit lxc-attach gelöst werden; neue stabile Veröffentlichung der Originalautoren; ABI-Version auf 18 angehoben; [rt] Aktualisierung auf 4.19.207-rt88; usb: hso: Fehlerbehandlungscode von hso_create_net_device überarbeitet [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q> (/proc/$pid/attr/-Öffner mm_struct nachverfolgen), damit die Probleme mit lxc-attach gelöst werden; neue stabile Veröffentlichung der Originalautoren; ABI-Version auf 18 angehoben; [rt] Aktualisierung auf 4.19.207-rt88; usb: hso: Fehlerbehandlungscode von hso_create_net_device überarbeitet [CVE-2021-37159]">
<correction mariadb-10.3 "Neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2021-2389 CVE-2021-2372]; Pfad der ausführbaren Perl-Datei in Skripten korrigiert">
<correction modsecurity-crs "Probleme mit Einschleusung via Anfragekörper behoben [CVE-2021-35368] [CVE-2021-35368]">
<correction node-ansi-regex "Auf reguläre Ausdrücke basierende Dienstblockade behoben [CVE-2021-3807]">
<correction node-axios "Auf reguläre Ausdrücke basierende Dienstblockade behoben [CVE-2021-3749]">
<correction node-jszip "NULL-Prototypen-Objekt für this.files verwenden [CVE-2021-23413]">
<correction node-tar "Verzeichnis-Cache von Nicht-Verzeichnissen säubern [CVE-2021-32803]; absolute Pfade gründlicher stutzen [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Einstellung von NVVMIR_LIBRARY_DIR auf ppc64el korrigiert">
<correction nvidia-graphics-drivers "Neue stabile Version der Originalautoren; Dienstblockaden behoben [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-driver-libs: <q>Empfiehlt</q> hinzugefügt: libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "Neue stabile Version der Originalautoren; Dienstblockade behoben [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-legacy-390xx-driver-libs: <q>Empfiehlt</q> hinzugefügt: libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "Neue Veröffentlichung der Originalautoren; Fehlplanung der wiederholten Anwendung eines Projektionsschritts behoben [CVE-2021-3677]; SSL-Wiederverhandlung vollständiger verbieten">
<correction proftpd-dfsg "<q>mod_radius leaks memory contents to radius server</q> (Speicherinhalte von mod_radius sickern zum radius-Server durch), <q>cannot disable client-initiated renegotiation for FTPS</q> (durch Client angestoßene Wiederverhandlung für FTPS kann nicht abgebrochen werden), Navigation in symbolisch verlinkte Verzeichnisse, mod_sftp-Absturz bei Verwendung von pubkey-auth mit DSA-Schlüsseln beseitigt">
<correction psmisc "Regression in killall, wegen der es Prozesse, deren Namen länger als 15 Zeichen waren, nicht mehr gefunden hat, entfernt">
<correction python-uflash "URL der Firmware aktualisiert">
<correction request-tracker4 "Timing-Seitenkanal im Anmeldesystem geschlossen [CVE-2021-38562]">
<correction ring "Dienstblockade in der eingebetteten Kopie von pjproject beseitigt [CVE-2021-21375]">
<correction sabnzbdplus "Verzeichnisausbruch in der Umbenennungsfunktion verhindert [CVE-2021-29488]">
<correction shim "arm64-Korrektur übernommen, um das Sektions-Layout zu frisieren und Absturzprobleme zu lösen; im unsicheren Modus nicht abbrechen, wenn die MokListXRT-Variable nicht erzeugt werden kann; bei grub-Installationsproblmen nicht abbrechen, sondern warnen">
<correction shim-helpers-amd64-signed "arm64-Korrektur übernommen, um das Sektions-Layout zu frisieren und Absturzprobleme zu lösen; im unsicheren Modus nicht abbrechen, wenn die MokListXRT-Variable nicht erzeugt werden kann; bei grub-Installationsproblemen nicht abbrechen, sondern warnen">
<correction shim-helpers-arm64-signed "arm64-Korrektur übernommen, um das Sektions-Layout zu frisieren und Absturzprobleme zu lösen; im unsicheren Modus nicht abbrechen, wenn die MokListXRT-Variable nicht erzeugt werden kann; bei grub-Installationsproblemen nicht abbrechen, sondern warnen">
<correction shim-helpers-i386-signed "arm64-Korrektur übernommen, um das Sektions-Layout zu frisieren und Absturzprobleme zu lösen; im unsicheren Modus nicht abbrechen, wenn die MokListXRT-Variable nicht erzeugt werden kann; bei grub-Installationsproblemen nicht abbrechen, sondern warnen">
<correction shim-signed "Probleme, die den Boot-Vorgang auf arm64 verhindern, durch Einbindung einer älteren, als funktionierend bekannten Version von unsigned shim auf jener Plattform umgangen; arm64 wieder so einstellen, dass ein aktuelles unsigniertes Kompilat verwendet wird; arm64-Korrektur übernommen, um das Sektions-Layout zu frisieren und Absturzprobleme zu lösen; im unsicheren Modus nicht abbrechen, wenn die MokListXRT-Variable nicht erzeugt werden kann; bei grub-Installationsproblemen nicht abbrechen, sondern warnen">
<correction shiro "Authentifizierungs-Umgehung behoben [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; Spring-Framework-Kompatibilitäts-Korrektur aktualisiert; Guice 4 unterstützen">
<correction tzdata "Sommerzeitregeln für Samoa und Joardanien aktualisiert; nicht stattfindende Schaltsekunde am 31.12.2021 bestätigt">
<correction ublock-origin "Neue stabile Version der Originalautoren; Dienstblockade abgestellt [CVE-2021-36773]">
<correction ulfius "Vor der Verwendung von Speicher sicherstellen, dass er initialisiert wird [CVE-2021-40540]">
<correction xmlgraphics-commons "Problem mit serverseitiger Abfragefälschung beseitigt [CVE-2020-11988]">
<correction yubikey-manager "Bei yubikey-manager fehlende Abhängigkeit von python3-pkg-resources nachgetragen">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Oldstable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheits-Team hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction birdtray "Inkompatibel mit neueren Thunderbird-Versionen">
<correction libprotocol-acme-perl "Unterstützt nur die obsolete ACME-Version 1">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Oldstable eingeflossen sind.
</p>


<h2>URLs</h2>

<p>Die vollständigen Listen von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt;, oder kontaktieren das Oldstable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>


