#use wml::debian::template title="Ezért válaszd a Debian-t" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"

#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Szabolcs Siebenhofer"

<p>Nagyon sok oka van, amiért a felhasználók a Debian-t választják operációs rendszerüknek.</p>

<h1>Jelentősebb indokok</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>A Debian Szbad szoftver.</strong></dt>
  <dd>
    A Debian szabadon készül és nyílt forráskódú szoftver és mindig 100%-ban
    <a href="free">szabad</a> lesz. Akárki használhatja, módosíthatja és
    terjesztheti. Ez a legfőbb ígéretünk a <a href="../users">felhasználóinknak</a>.
    És nem kerül semmibe.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian stabil és biztonságos Linux alapú operációs rendszer.</strong></dt>
  <dd>
    A Debian egy operációs rendszer nagyon sokféle eszközre, így laptopokra, asztali
    gépekre és szerverekre. A felhasználók kedvelik a stabilitását és megbízhatóságát
    1993 óta. Ésszerú alapbeállításokat biztosítunk minden egyes csomaghoz. A Debian
    fejlesztői biztonsági frissítéseket biztosítanak minden egyes csomaghoz az életciklusuk
    alatt, amikor csak lehetséges.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian kiterjedt hardvertámogatással rendelkezik.</strong></dt>
  <dd>
    A legtöbb hardvert már a Linux kernel támogatja. Szabadalmaztatott illesztőporgramok
    elérhetőek, amennyiben a szabad szoftverek nem felelnek meg.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian zökkenőmentes frissítéseket nyújt.</strong></dt>
  <dd>
    A Debian jól ismert a egyszerű és sima frissítéseiről a kiadási ciklusok alatt,
    de a verzióváltásoknál is.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian a magja és alapja sok más kiadásnak.</strong></dt>
  <dd>
    Sok népszerű Linux kiadás, mint az Ubuntu, a Knoppix, a PureOS, a SteamOS vagy 
    Tails, a Debian-t választotta szoftvere alapjául.
    A Debian mindenlki számára biztosítja az eszközöket, így bárki ki tudja bővíteni
    a Debian archívumvól származó csomagokat a igényeihez szükséges saját csomagokkal.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian projekt közösség.</strong></dt>
  <dd>
    A Debian nem csak operációs rendszer. A szoftver közös munkája több száz 
    önkéntesnek, szerte a világon. Te is részese lehetsz ennek a közösségnek,
    még ha nem is vagy porgramozó vagy rendszer adminisztrátor. A Debian-t a 
    közösség és a konszezus irányítja és 
    <a href="../devel/constitution">demokratikus irányítási struktúrával</a>
    rendelkezik.
    Mivel minden Debian fejlesztő egyforma jogokkal rendelkezik, így egy Vállalat
    álat sincsen befolyásolva. Több, mint 60 országból vannak fejlesztőink és több, 
    mint 80 nyelv támogatott a Debian Telepítőnkben.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian többféle telepítési lehetőséggel rendelkezik.</strong></dt>
  <dd>
    A végfelhasználók használhatják a 
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>-t,
    ami tartalmazza a könnyen használható Calamares telepítőt, amihez nagyon kevés beavatkozásra
    vagy előzetes ismeretre van szüksége. A gyakorlottabb felhasználók az egyedi teljes értékű 
    telepítőt használhatják, míg a szakértők finomhangolhatják a telepítést vagy akár igénybe vehetik
    a teljesen automatizált hálózati telepítő eszközt.
  </dd>
</dl>

<br>

<h1>Vállalati környezet</h1>

<p>
  If you need Debian in a professional environment, you may enjoy these
  additional benefits:
  Ha a Debian-ra professzionális környezetben van szükséged, a további előnyöket is 
  élvezheted:
</p>

<dl>
  <dt><strong>A Debian megbízható.</strong></dt>
  <dd>
    A Debian bizonyítja megbízhatóságát minden nap több száz valós élethelyzetben,
    az egyszerű laptopoktól a szuper ütköztetőkig, tőzsdéken és az autóiparban.
    Szintén népszerű az oktatásban, a tudományban és az állami szektorban.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian-nak nagyon sok szakértője van.</strong></dt>
  <dd>
    A csomagok karbantartói nem csak gondját viselik a Debian csomagoknak és 
    részt vesznek az új kiadások létrejöttében. Gyakran szakértőikaz eredeti 
    szoftvernek és közvetlenül támogatják annak fejlesztését. Néha részei is 
    az eredeti szoftver fejlesztőcsapatának.
  </dd>
</dl>

<dl>
  <dt><strong>A Debian biztonságos.</strong></dt>
  <dd>
    A Debian rendelkezik biztonsági támogatással a stabil kiadáshoz. Sok más
    disztribúció és biztonsági kutató a Debian biztonsági nyomkövetőjére támaszkodik.
  </dd>
</dl>

<dl>
  <dt><strong>Hosszútávú Támogatás.</strong></dt>
  <dd>
    <a href="https://wiki.debian.org/LTS">Hosszú Távú Támogatás</a>
    (Long Term Support - LTS) költségek nélkül. Ez 5 évre kiterjesztett támogatást és mást 
    nyújt a stabil verzióhoz. This brings you extended support for the stable
    release for 5 years and more. Ezen kívül létezik a 
    <a href="https://wiki.debian.org/LTS/Extended">Kiterjesztett LTS</a> kezdeményezés,
    ami kiterjesztett támogatást biztosít bizonyos csomagoknak 5 évnél tovább.
  </dd>
</dl>

<dl>
  <dt><strong>Felhő képek.</strong></dt>
  <dd>
    Hivatalos felhő képek érhetőek el minden fontos felhő platformhoz. Biztosítjuk 
    az eszközöket és konfigurációt, így magad tudod megépíteni a saját személyreszabott
    felhő képedet. A Debian szintén használható virtuális gépeken vagy egy konténerben.
  </dd>
</dl>

<br>

<h1>Fejlesztők</h1>
<p>Debian is widely used by every kind of software and hardware developers.</p>

<dl>
  <dt><strong>Nyilvánosan elérhető hibakövető rendszer.</strong></dt>
  <dd>
    A Debian <a href="../Bugs">Hiba követő rendszer</a> (Bug tracking system - BTS)
    bárki számára elérhető web böngészőn keresztül. Nem rejtjük el a szoftverhibákat
    és könnyen tudsz hibajelentéseket küldeni.
  </dd>
</dl>

<dl>
  <dt><strong>IoT és beágyazott eszközök.</strong></dt>
  <dd>
    Az eszközök sok fajtáját támogatjuk, úgy mint Raspberry Pi, a QNAP
    variánsai, mobil eszközök, otthoni routerek és nagyon sok egykártyás 
    számítógépet (Single Board Computer - SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Több hardver architektúra.</strong></dt>
  <dd>
    <a href="../ports">Hosszú a listája</a> a támogatott CPU architektúráknak,
    így amd64, i386, az ARM és MIPS különféle verziói, POWER7, POWER8, IBM System Z,
    RISC-V. A Debian régebbi és különleges architektúrákhoz is elérhető.
  </dd>
</dl>

<dl>
  <dt><strong>Hatalmas mennyiségű szoftvercsomag érhető el.</strong></dt>
  <dd>
    A Debian-nak van a legnagyobb számú telepítő csomagja (jelenleg 
    <packages_in_stable>). A csomagjaink deb formátumot használnak, ami jól
    ismert magas mnőségéről.
  </dd>
</dl>

<dl>
  <dt><strong>Választási lehetőség a kiadások között.</strong></dt>
  <dd>
    A stabil kiadás mellett, megszerezheted a szoftverek újabb verzóját 
    a testing vagy unstable kiadásokból.
  </dd>
</dl>

<dl>
  <dt><strong>Magas minőség a fejlesztői eszközök és politika által.</strong></dt>
  <dd>
    Néhány fejlesztői eszköz segít megtartani a magas minődéset és a 
    <a href="../doc/debian-policy/">politikánk</a> definálja a technikai
    követelményeket, aminek minden csomagnak meg kell felelnie, hogy bekerüljön a 
    disztribúcióba. A folyamatos integráció során az autopkgtest szoftvert futtatjuk,
    a piuarts a mi telepítési, frissítési és eltávolítási teszt eszközünk és a
    lintian pedig az átfogó Debian csomag ellenőrző eszközünk.
  </dd>
</dl>

<br>

<h1>Mit mondanak a felhasználóink</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Számomra az egyszerű használat és stabilistás tökéletes szintje. Már sok
      különféle disztribúciót használtam az évek során, de a Debian az egyetlen,
      ami egyszerűen csak működött.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Szikla szilárd. Rengeteg csomag. Kitűnő közösség.
    </strong></q>
  </li>

  <li>
    <q><strong>
      A Debian számomra a könnyű használat és stabilitás szimbóluma.
    </strong></q>
  </li>
</ul>

