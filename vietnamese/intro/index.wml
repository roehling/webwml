#use wml::debian::template title="Giới thiệu về Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Trần Ngọc Quân"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian là một Cộng đồng của những con người</h2>
      <p>Hàng ngàn tình nguyện viên trên toàn thế giới làm việc cùng nhau trên hệ điều hành Debian, ưu tiên Phần mềm tự do nguồn mở. Gắn bó với nhau thành dự án Debian.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Con người:</a>
          Chúng tôi là ai và làm gì
        </li>
        <li>
          <a href="philosophy">Triết lý sống của chúng tôi:</a>
          Tại sao chúng tôi làm thế và chúng tôi làm như thế nào
        </li>
        <li>
          <a href="../devel/join/">Muốn tham gia:</a>
          Cách để trở thành người đóng góp cho Debian
        </li>
        <li>
          <a href="help">Đóng góp:</a>
          Cách bạn có thể giúp đỡ Debian
        </li>
        <li>
          <a href="../social_contract">Khế ước xã hội Debian:</a>
          Chương trình nghị sự đạo đức của chúng tôi
        </li>
        <li>
          <a href="diversity">Mọi người đều được chào đón:</a>
          Tuyên bố về tính đa dạng của Debian
        </li>
        <li>
          <a href="../code_of_conduct">Dành cho những người tham gia:</a>
          Quy tắc ứng xử của Debian
        </li>
        <li>
          <a href="../partners/">Đối tác:</a>
          Các công ty và tổ chức hỗ trợ dự án Debian
        </li>
        <li>
          <a href="../donations">Quyên góp:</a>
          Cách quyên góp cho dự án Debian
        </li>
        <li>
          <a href="../legal/">Thông tin Pháp luật:</a>
          Giấy phép, thương hiệu, chính sách riêng tư, chính sách cha mẹ, v.v..
        </li>
        <li>
          <a href="../contact">Liên hệ:</a>
          Cách thức để liên hệ với chúng tôi
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian là một Hệ điều hành</h2>
      <p>Debian là một hệ điều hành tự do, được phát triển và bảo trì bởi dự án Debian. Một bản phân phối Linux tự do với hàng ngàn ứng dụng phù hợp với mong muốn của người dùng.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Tải về:</a>
          Lấy Debian về từ đâu
        </li>
        <li>
          <a href="why_debian">Sao lại là Debian:</a>
          Lý do nên dùng Debian
        </li>
        <li>
          <a href="../support">Hỗ trợ:</a>
          Tìm trợ giúp ở đâu
        </li>
        <li>
          <a href="../security">Bảo mật:</a>
          Cập nhật lần cuối <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Phần mềm:</a>
          Tìm và duyệt các gói phần mềm của Debian
        </li>
        <li>
          <a href="../doc">Tài liệu:</a>
          Hướng dẫn cài đặt, FAQ, HOWTOs, Wiki, và nhiều thứ khác nữa
        </li>
        <li>
          <a href="../bugs">Hệ thống theo dõi lỗi (BTS):</a>
          Cách để bóa cáo lỗi, tài liệu BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">Các bó thư:</a>
          Tập hợp các bó thư Debian cho người dùng, nhà phát triển v.v..
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Các siêu gói cho các mục đích đặc thù
        </li>
        <li>
          <a href="../devel">Góc phát triển:</a>
          Thông tin đáng chú ý nhất dành cho những nhà phát triển Debian
        </li>
        <li>
          <a href="../ports">Chuyển đổi/Kiến trúc:</a>
          Debian hỗ trợ nhiều kiến trúc CPU khác nhau
        </li>
        <li>
          <a href="search">Tìm kiếm:</a>
          Thông tin về cách làm thế nào để sử dụng bộ máy tìm kiếm Debian
        </li>
        <li>
          <a href="cn">Ngôn ngữ:</a>
          Cài đặt ngôn ngữ cho trang thông tin điện tử Debian
        </li>
      </ul>
    </div>
  </div>

</div>

