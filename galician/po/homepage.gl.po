# Parodper <parodper@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-07-15 15:57+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "O Sistema Operativo Universal"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf está en marcha!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Logo da DebConf"

#: ../../english/index.def:19
#, fuzzy
#| msgid "DC19 Group Photo"
msgid "DC22 Group Photo"
msgstr "Foto Grupal da DC19"

#: ../../english/index.def:22
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf22 Group Photo"
msgstr "Foto Grupal da DebConf19"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:29
#, fuzzy
#| msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Foto Grupal da MiniDebConf de Regensburgo en 2021"

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Regensburgo 2021"

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Foto Grupal da MiniDebConf de Regensburgo en 2021"

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Captura do instalador Calamares"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Unha captura de pantalla do instalador Calamares"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian é coma unha navalla suiza"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "A xente goza con Debian"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Compañeiros de Debian na Debconf18 en Hsinchu pasándoa en grande"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Anacos de Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Bitácora"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronovas"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronovas de Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planeta"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "O Planeta de Debian"
