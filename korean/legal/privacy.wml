#use wml::debian::template title="개인정보 정책" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="3c6ffb2bcd885b73ca6311b72caa3ab54fdaeea1" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

## Translators may want to add a note stating that the translation 
## is only informative and has no legal value, and people
## should look at the original English for that. 
## Some languages have already put such a note in the
## translations of /trademark and /license, for example.

<p>주의: 이 번역은 법적 효력이 없습니다. 영어 원문만 법적 효력이 있습니다.</p>

<p><a href="https://www.debian.org/">데비안 프로젝트</a>는 자유 운영체제를 만들려는 개인 봉사자 모임입니다.</p>

<p>
데비안을 사용하려는 사람 누구도 개인 정보를 제공할 필요는 없습니다.
프로젝트에 의해 운영되는 공식 미러와 수많은 제3자로부터 등록이나 다른 형태의 신분증 없이 자유롭게 다운로드 할 수 있습니다.</p>

<p>그러나 데비안 프로젝트와 상호 작용하는 다양한 측면에는 개인정보 수집을 포함합니다.
이것은 주로 프로젝트가 받은 이름과 이메일 주소 형식입니다.
모든 데비안 메일링리스트는 버그 추적 시스템과의 모든 상호 작용과 같이 공개적으로 보관됩니다.
이것은 우리의 <a
href="https://www.debian.org/social_contract">사회 계약</a>, 특히 자유 소프트웨어 공동체 (#2)에게 돌려주고,
우리의 문제를 숨기지 않을 것(#3)과 일치합니다.
보유하고 있는 정보에 대해서는 추가 처리를 수행하지 않지만, 전자메일이 목록에 자동으로 공유되거나 버그 추적 시스템과의 상호 작용과 같은 제3자와 자동으로 공유될 수 있습니다.
</p>

<p>아래 목록은 프로젝트에서 실행되는 다양한 서비스, 해당 서비스에서 사용하는 정보 및 필요한 이유를 분류합니다.
</p>

<p><strong>debian.net</strong> 도메인 아래의 호스트와 서비스는 공식 데비안 프로젝트의 일부가 아닙니다.
그들은 프로젝트 자체보다는 프로젝트와 관련있는 개인이 운영합니다. 
해당 서비스가 보유한 데이터에 대한 질문은 데비안 프로젝트 자체가 아닌 서비스 소유자에게 해야합니다.
</p>

<h2>기여자 (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>데비안 기여자 사이트는 버그 보고서 제출, 아카이브에 업로드, 메일링 리스트에 게시 또는 
프로젝트와의 다양한 기타 상호 작용을 통해 누가 어디에서 데비안 프로젝트에 기여했는지 데이터를 제공합니다.
해당 서비스로부터 정보를 받고 (로그인 이름 및 마지막 기여 시간과 같은 식별자에 대한 세부 사항) 
프로젝트가 개인에 대한 정보를 저장하는 위치를 볼 수있는 단일 참조 지점을 제공합니다.</p>

<h2>아카이브 (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>데비안의 주요 배포 방법은 공개 아카이브 네트워크를 통해서입니다.
이 아카이브는 모든 바이너리 패키지와 관련 소스 코드로 구성되며, 여기에는 변경 로그, 저작권 정보 및
일반 문서의 일부로 저장된 이름 및 전자 메일 주소 형식의 개인 정보가 들어갑니다.
이 정보의 대부분은 업스트림 소프트웨어 제작자 배포 소스 코드를 통해 제공되며, 
데비안은 라이선스 및 문서를 추적하고 데비안 자유 소프트웨어 가이드라인을 지키기 위해 라이선스 및 저작권을 추적하기 위한 정보를 추가합니다.
</p>

<h2>버그 추적 시스템 (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>버그 추적 시스템은 이메일을 통해 상호작용하며 버그와 관련된 모든 이메일을 버그 기록의 일부로 저장합니다.
프로젝트가 배포판에서 발견된 문제를 효과적으로 처리하고 사용자가 해당 문제에 대한 세부 정보를 보고 수정 또는
해결 방법을 사용할 수 있도록 하기 위해 버그 추적 시스템 전체를 공개적으로 액세스 할 수 있습니다.
따라서 BTS로 전송되는 이메일 헤더의 일부로 이름 및 이메일 주소를 포함한 모든 정보가 보관되고 공개됩니다.
</p>

<h2>데브컨프 (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>데브컨프 등록 구조는 컨퍼런스 참석자의 세부 사항을 저장합니다.
이것은 장학금 자격, 프로젝트와의 연관성을 결정하고 참석자에게 적절한 세부 사항을 문의하기 위해 필요합니다.
또한 컨퍼런스에 공급 업체와 공유될 수 있습니다.
예를 들어 회의에 참석한 참석자는 숙박 시설과 이름 및 출석 날짜를 공유합니다.
</p>

<h2>개발자 LDAP (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>데비안 인프라 내의 머신에 대한 계정 액세스 권한이있는 프로젝트 기여자 (개발자 및 게스트 계정이 있는 기타 사용자)는 
프로젝트의 LDAP 인프라 내에 세부 사항이 저장됩니다.
이것은 주로 이름, 사용자 이름 및 인증 정보를 저장합니다.
또한 제공자가 성별, 인스턴트 메시징 (IRC/XMPP), 국가 및 주소 또는 전화 정보와 같은 추가 정보를 제공하고 
휴가중이라면 메시지를 제공할 수 있는 옵션 기능도 제공합니다.
</p>

<p>웹 인터페이스 또는 LDAP 검색을 통해 이름, 사용자 이름 및 자발적으로 제공되는 세부 정보를 자유롭게 사용할 수 있습니다.
추가 정보는 데비안 인프라에 대한 계정 액세스 권한이 있는 다른 개인에게만 공유되며 
프로젝트 참여자가 이러한 연락처 정보를 교환할 수있는 중앙 위치를 제공하기 위한 것입니다.
어떤 시점에서도 명시적으로 수집되지 않으며 db.debian.org 웹 인터페이스에 로그인하거나 서명된 이메일을 이메일 인터페이스로 보내면 언제든지 제거할 수 있습니다.
자세한 내용은 <a href="https://db.debian.org/">https://db.debian.org/</a> 및 
<a href="https://db.debian.org/doc-general.html">https://db.debian.org/doc-general.html</a>을 참조하십시오.
</p>

<h2>Gitlab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>salsa.debian.org는 <a
href="https://about.gitlab.com/">GitLab</a> DevOps 라이프 사이클 관리 도구의 인스턴스를 제공합니다. 
이 프로젝트는 주로 프로젝트 기여자가 Git을 사용하여 소프트웨어 저장소를 호스팅하고 
기여자 간 협업을 장려할 수 있도록 사용합니다. 
결과적으로 계정을 관리하려면 다양한 개인정보가 필요합니다. 
프로젝트 멤버의 경우 중앙 데비안 LDAP 시스템에 연결되어 있지만 게스트도 계정을 등록할 수 있으며 
해당 계정의 설정 및 사용을 쉽게 하기 위해 이름과 이메일 세부 정보를 제공해야 합니다.
</p>

<p>Due to the technical nature of git contributions to the git repositories
held on salsa will contain the name and email address recorded within those git
commits. The chained nature of the git system means that any modification to
these commit details once they are incorporated into the repository is
extremely disruptive and in some cases (such as when signed commits are in use)
impossible.</p>

<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>Gobby is a collaborative online text editor, which tracks contributions and
changes against connected users. No authentication is required to connect to
the system and users may choose any username they wish. However while no
attempt is made by the service to track who owns usernames it should be
understand that it may prove possible to map usernames back to individuals
based upon common use of that username or the content they post to a
collaborative document within the system.</p>

<h2>메일링 리스트 (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>Mailing lists are the primary communication mechanism of the Debian Project.
Almost all of the mailing lists related to the project are open, and thus
available for anyone to read and/or post to. All lists are also archived; for
public lists this means in a web accessible manner. This fulfils the project
commitment to transparency, and aids with helping our users and developers
understand what is happening in the project, or understand the historical
reasons for certain aspects of the project. Due to the nature of email these
archives will therefore potentially hold personal information, such as names
and email addresses.</p>

<h2>새 회원 사이트 (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>Contributors to the Debian Project who wish to formalise their involvement
may choose to apply to the New Members process. This allows them to gain the
ability to upload their own packages (via Debian Maintainership) or to become
full voting members of the Project with account rights (Debian Developers, in
uploading and non-uploading variants). As part of this process various personal
details are collected, starting with name, email address and
encryption/signature key details. Full Project applications also involve the
applicant engaging with an Application Manager who will undertake an email
conversation to ensure the New Member understands the principles behind Debian
and has the appropriate skills to interact with the Project infrastructure.
This email conversation is archived and available to the applicant and
Application Managers via the nm.debian.org interface. Additionally details of
outstanding applicants are publicly visible on the site, allowing anyone to see
the state of New Member processing within the Project to ensure an appropriate
level of transparency.</p>

<h2>Popularity Contest (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p>"popcon" tracks which packages are installed on a Debian system, to enable
the gathering of statistics about which packages are widely used and which are
no longer in use. It uses the optional "popularity-contest" package to collect
this information, requiring explicit opt-in to do so. This provides useful
guidance about where to devote developer resources, for example when migrating
to newer library versions and having to spend effort on porting older
applications. Each popcon instance generates a random 128 bit unique ID which
is used to track submissions from the same host. No attempt is made to map this
to an individual about submissions are made via email or HTTP and it is thus
possible for personal information to leak in the form of the IP address used
for access or email headers. This information is only available to the Debian
System Administrators and popcon admins; all such meta-data is removed before
submissions are made accessible to the project as a whole. However users should
be aware that unique signatures of packages (such as locally created packages
or packages with very low install counts) may make machines deducible as
belonging to particular individuals.</p>

<p>Raw submissions are stored for 24 hours, to allow replaying in the event of
issues with the processing mechanisms. Anonymized submissions are kept for at
most 20 days. Summary reports, which contain no personally identifiable
information, are kept indefinitely.</p>

<h2>snapshot (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>The snapshot archive provides a historical view of the Debian archive
(ftp.debian.org above), allowing access to old packages based on dates and
version numbers. It carries no additional information over the main archive
(and can thus contain personal information in the form of names + email address
within changelogs, copyright statements and other documentation), but can
contain packages that are no longer part of shipping Debian releases. This
provides a useful resource to developers and users when tracking down
regressions in software packages, or providing a specific environment to run a
particular application.</p>

<h2>Votes (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>The vote tracking system (devotee) tracks the status of ongoing General
Resolutions and the results of previous votes. In the majority of cases this
means that once the voting period is over details of who voted (usernames +
name mapping) and how they voted becomes publicly visible. Only Project
members are valid voters for the purposes of devotee, and only valid votes are
tracked by the system.</p>

<h2>위키 (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>The Debian Wiki provides a support and documentation resource for the
Project which is editable by everyone. As part of that contributions are
tracked over time and associated with user accounts on the wiki; each
modification to a page is tracked to allow for errant edits to be reverted and
updated information to be easily examined. This tracking provides details of
the user responsible for the change, which can be used to prevent abuse by
blocking abusive users or IP addresses from making edits. User accounts also
allow users to subscribe to pages to watch for changes, or see details of
changes throughout the entire wiki since they last checked. In general user
accounts are named after the name of the user, but no validation is performed
of the account names and a user may choose any free account name. An email
address is required for the purposes of providing a mechanism for account
password reset, and notifying the user of any changes on pages they are
subscribed to.</p>

<h2>Echelon</h2>

<p>Echelon is a system used by the Project to track member activity; in
particular it watches the mailing list and archive infrastructures, looking for
posts and uploads to record that a Debian member is active. Only the most
recent activity is stored, in the member's LDAP record. It is thus limited to
only tracking details of individuals who have accounts within the Debian
infrastructure. This information is used when determining if a project member
is inactive or missing and thus that there might be an operational requirement
to lock their account or otherwise reduce their access permissions to ensure
Debian systems are kept secure.</p>

<h2>Service related logging</h2>

<p>In addition to the explicitly listed services above the Debian
infrastructure logs details about system accesses for the purposes of ensuring
service availability and reliability, and to enable debugging and diagnosis of
issues when they arise. This logging includes details of mails sent/received
through Debian infrastructure, web page access requests sent to Debian
infrastructure, and login information for Debian systems (such as SSH logins to
project machines). None of this information is used for any purposes other than
operational requirements and it is only stored for 15 days in the case of web
server logs, 10 days in the case of mail log and 4 weeks in the case of
authentication/ssh logs.</p>
