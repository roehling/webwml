#use wml::debian::template title="Stap 4: Taken en Vaardigheden" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>De informatie op deze pagina, hoewel openbaar, zal voornamelijk van belang
zijn voor toekomstige Debian-ontwikkelaars.</p>

<h2>Stap 4: Taken en Vaardigheden</h2>

<p>De meeste huidige leden van het <a href="./newmaint#DebianProject">
Debian Project</a> onderhouden één of meer pakketten voor de distributie.
Er zijn echter veel andere taken die gedaan moeten worden die geen betrekking
hebben op pakketbeheer.</p>

<p>De <a href="./newmaint#AppMan">kandidatuurbeheerder</a> (Application
Manager - AM) zal samen met de <a href="./newmaint#Applicant">kandidaat</a>
bepalen welke taken de kandidaat als vrijwilliger zal uitvoeren. Daarna zal de
kandidaat zijn vaardigheden op dit gebied moeten aantonen.</p>

<p>De volgende taken zijn voor de hand liggende voorbeelden van de verschillende
taken die beschikbaar zijn voor de kandidaat, maar zij omvatten niet
noodzakelijk alles wat een kandidaat interessant en productief kan vinden voor
de groep. Aanvullende taken kunnen worden vastgesteld door het AM en de
kandidaat.</p>

<p>Enkele voorbeelden van taken zijn:</p>

<ul>
 <li><h4>Pakketbeheer</h4>
  Door een pakket te onderhouden kan een kandidaat-ontwikkelaar laten zien dat
  hij/zij het <a href="$(DOC)/debian-policy/">Beleid van Debian</a> begrijpt en
  hoe hij/zij samenwerkt met gebruikers van Debian en indieners van bugs.
 </li>

 <li><h4>Documentatie</h4>
  De kandidaat kan zijn vaardigheden op dit gebied aantonen door man-pagina's te
  schrijven voor uitvoerbare bestanden die er geen hebben, door een verouderd
  document bij te werken en door nieuwe documentatie te aan te maken die door
  gebruikers wordt vereist, maar die nog ontbreekt in de distributie.
 </li>

 <li><h4>Debuggen, testen en patchen</h4>
  De kandidaat kan vaardigheden op dit gebied tonen door samen met het QA-team
  te werken aan het oplossen van bugs, of door samen met het testteam
  installatieprocessen of individuele pakketten te testen. De kandidaat kan bugs
  repareren in bestaande Debian-pakketten of bugrapporten indienen in het Debian
  BTS waarin problemen worden beschreven en patches worden bijgevoegd.
 </li>
</ul>

<p>De kandidaat en de kandidatuurbeheerder kunnen alternatieve demonstratietaken
uitwerken. Dergelijke alternatieve taken moeten uitgewerkt worden in coördinatie
met de <a href="./newmaint#FrontDesk">frontdesk</a>
en de <a href="./newmaint#DAM">accountbeheerder van Debian</a>.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
