#use wml::debian::template title="Debian verkrijgen"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="57878bd6a7da08f6899198d6d830e614c4deebbb"

<p>Debian wordt <a href="../intro/free">op een vrije manier</a>
verspreid via het Internet. U kunt alles downloaden van een van onze <a
href="ftplist">spiegelservers</a>.
De <a href="../releases/stable/installmanual">installatiehandleiding</a> bevat
gedetailleerde installatie-instructies.
En de aantekeningen bij de release zijn <a href="../releases/stable/releasenotes">hier</a> te vinden.
</p>

<p>Deze pagina heeft opties voor het installeren van Debian Stable. Als u
geïnteresseerd bent in Testing of Unstable, bezoek dan onze
<a href="../releases/">releases-pagina</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Een installatie-image downloaden</a></h2>
    <p>
      Afhankelijk van uw internetverbinding, kunt u één van de volgende
      bestanden downloaden:
    </p>
   <ul>
     <li>Een <a href="netinst"><strong>klein installatie-image</strong></a>:
          kan snel gedownload worden en moet op een verwijderbare schijf geschreven
          worden. Om dit te kunnen gebruiken, heeft uw machine een internetverbinding
          nodig.
      <ul class="quicklist downlist">
        <li><a title="Het installatieprogramma voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bits
            pc netinst iso</a></li>
        <li><a title="Het installatieprogramma voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bits
            pc netinst iso</a></li>
      </ul>
     </li>
     <li>Een groter <a href="../CD/"><strong>compleet
      installatie-image</strong></a>: bevat meer pakketten, zodat het eenvoudiger te
      installeren is op een machine zonder internetverbinding.
      <ul class="quicklist downlist">
        <li><a title="Dvd-torrents voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/bt-dvd/">64-bits pc torrents (dvd)</a></li>
        <li><a title="Dvd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/bt-dvd/">32-bits pc torrents (dvd)</a></li>
        <li><a title="Cd-torrents voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/bt-cd/">64-bits pc torrents (cd)</a></li>
        <li><a title="Cd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/bt-cd/">32-bits pc torrents (cd)</a></li>
      </ul>
     </li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Een Debian cloudimage gebruiken</a></h2>
    <p>Een officieel <a href="https://cloud.debian.org/images/cloud/"><strong>cloudimage</strong></a>,
            gebouwd door het cloudteam, kan gebruikt worden op:</p>    <ul>
      <li>uw OpenStack provider, in qcow2 formaat of raw formaat.
      <ul class="quicklist downlist">
	   <li>64-bits AMD/Intel (<a title="OpenStack image voor 64-bits AMD/Intel qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits AMD/Intel raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.raw">raw</a>)</li>
       <li>64-bits ARM (<a title="OpenStack image voor 64-bits ARM qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits ARM raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.raw">raw</a>)</li>
	   <li>64-bits Little Endian PowerPC (<a title="OpenStack image voor 64-bits Little Endian PowerPC qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits Little Endian PowerPC raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, ofwel als een machine-image of via de AWS Marktplaats.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon machine-images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon machine-images</a></li>
	    <li><a title="AWS Marktplaats" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplaats</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, op de Azure Marktplaats.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 op Azure Marktplaats" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
        <li><a title="Debian 11 op Azure Marktplaats" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Een set cd's of dvd's kopen van een
      leverancier die Debian cd's verkoopt</a></h2>

   <p>
      Veel aanbieders verkopen de distributie voor minder dan 5&nbsp;euro
      (plus porto). (ga op hun webpagina na of ze ook internationaal leveren).
      <br />
      Bij sommige van de <a href="../doc/books">boeken over Debian</a> krijgt
      u ook een gratis cd.
   </p>

   <p>Dit zijn de belangrijkste voordelen van cd's::</p>

   <ul>
     <li>Installatie vanaf cd's is meer recht-toe-recht-aan.</li>
     <li>Het is mogelijk om te installeren op machines zonder
     internetverbinding.</li>
	 <li>U kunt Debian installeren (op zoveel machines als u wilt) zonder zelf
     alle pakketten te hoeven downloaden.</li>
     <li>De cd kan worden gebruikt om gemakkelijk een beschadigd
     Debian-systeem te repareren.</li>
   </ul>

   <h2><a href="pre-installed">Een computer kopen met Debian geïnstalleerd</a></h2>
   <p>Dit heeft een aantal voordelen:</p>
   <ul>
    <li>U hoeft Debian niet te installeren.</li>
    <li>De installatie is vooraf geconfigureerd voor de juiste hardware.</li>
    <li>Mogelijk biedt de leverancier technische ondersteuning.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Debian live uitproberen vóór het installeren</a></h2>
    <p>
      U kunt Debian uitproberen door een live-systeem te starten
      vanaf een cd, dvd of USB-stick, zonder bestanden op de computer te
      installeren. Wanneer u klaar bent, kunt u het bijgevoegde
      installatieprogramma (vanaf Debian 10 Buster is dit het
      gebruiksvriendelijke  <a href="https://calamares.io">Calamares
      installatieprogramma</a>) uitvoeren. In de veronderstelling dat deze
      images beantwoorden aan uw vereisten inzake grootte, taal en
      pakketselectie, is dit mogelijk een methode die geschikt is voor u.
      Lees meer uitgebreide <a
      href="../CD/live#choose_live">informatie over deze methode</a> om u
      in staat te stellen om te beslissen.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Live-torrents voor 64-bits Intel en AMD pc's downloaden"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bits pc live-torrents</a></li>
    </ul>
  </div>

</div>

