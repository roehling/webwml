#use wml::debian::template title="Materiaal en reclamegoederen voor evenementen van Debian" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="80acf05cbcfdae22553c48bd354f7d168458bd10"

<p>Deze pagina bevat een lijst met al het reclamemateriaal dat mensen die
Debian-stands organiseren kunnen gebruiken (zodat ze dit niet helemaal opnieuw
hoeven te maken):</p>

<toc-display />


<toc-add-entry name="posters">Posters</toc-add-entry>

<p>Posters kleden uw stand mooi aan. Neem dus de tijd om er een paar af te
drukken. Als u de juiste voorzorgsmaatregelen neemt, kunnen ze af en toe op
verschillende evenementen worden gebruikt. Als u ze niet wilt houden, kunt u ze
altijd weggeven tijdens een informele bijeenkomst in de marge van een
of andere conferentie (bij een zogenaamde BoF - Birds of a Feather).
</p>

<p><a href="mailto:arti@kumria.com">Arti Kumria</a> ontwierp de volgende
posters voor <a href="http://www.linuxexpo.com.au/">LinuxExpo Australia</a>,
dat in maart 2000 in Sydney plaats vond:</p>

<ul>
  <li><a href="materials/posters/poster1.pdf">poster1.pdf</a> [182 kB]
  <li><a href="materials/posters/poster1.ps.gz">poster1.ps.gz</a> [16.3 MB]
  <li><a href="materials/posters/poster2.pdf">poster2.pdf</a> [175 kB]
  <li><a href="materials/posters/poster2.ps.gz">poster2.ps.gz</a> [16.3 MB]
</ul>

<p>De voorbeeld-PDF-bestanden geven een goede weergave van hoe de poster
eruitziet, behalve dat de Debian-wervel roze wordt in plaats van rood. Het ziet
er wel goed uit als het wordt afgedrukt (maar zorg ervoor dat u eerst een
drukproef krijgt).</p>

<p>De bovenstaande posters werden vrijgegeven in het publieke domein. Als u de
originele gegevens wilt verkrijgen die zijn gebruikt om ze te maken, neem dan
contact op met
<a href="mailto:akumria@debian.org">Anand Kumria</a>.</p>

<p>Voor Duitsland heeft <a href="mailto:joey@debian.org">Joey</a> enkele
<a href="https://cvs.infodrom.org/goodies/posters/?cvsroot=debian">posters</a>
die hij u kan toesturen via de gewone post, maar alleen in bruikleen en alleen
voor een Debian-stand op een tentoonstelling.</p>

<p>Voor de Verenigde Staten heeft Don Armstrong een aantal spandoeken.</p>

<p>Thimo Neubauer zorgde ook voor een
<a href="materials/posters/worldmap/">wereldkaart</a> met de coördinaten van de
ontwikkelaars van Debian.</p>

<p>Ook Alexis Younes van 73lab.com ontwierp een aantal erg mooie posters.
Men kon ze downloaden van zijn website (maar de downloadlink is niet meer
beschikbaar sinds 29-08-2020). In Duitsland zijn enkele afgedrukte posters
beschikbaar. Vraag ernaar op de lijst
 <a href="mailto:debian-events-eu@lists.debian.org">debian-events-eu</a>.
Terloops: zij verkopen zeer goed, dus misschien wil u er wel wat meer
afdrukken :)</p>

<toc-add-entry name="flyers">Folders en brochures</toc-add-entry>

<p>Mensen nemen graag dingen mee van een stand, en zelfs als u het zich niet
kan veroorloven om duurdere dingen weg te geven (zie hieronder), kunt u altijd
folders voorzien. Folders kunnen veel vragen over Debian beantwoorden, ze
kunnen mensen overhalen om bij te dragen en omdat het papier is, kan men er
aantekeningen op maken! Al bij al zijn ze behoorlijk nuttig.</p>

<p>Bronmateriaal voor een meertalige folder, evenals instructies voor het
afdrukken zijn beschikbaar op de
<a href="https://debian.pages.debian.net/debian-flyers/">webpagina van Debian
in verband met folders</a>.</p>

<p>Voor brochures zijn <a href="materials/pamphlets/">AbiWord-bestanden</a>
beschikbaar in het Engels en het Spaans, die ook gemakkelijk vertaald kunnen
worden naar andere talen.</p>

<p>Voor Europa onderhoudt Michael Meskes een gedrukte versie van tweetalige
(Engels en Duits) folders die door Debian-mensen zijn gemaakt tijdens de
voorbereiding van <a href="2002/0606-linuxtag">LinuxTag 2002</a>. Zelfs als uw
evenement plaatsvindt in een land waarvan geen van de bovenstaande talen de
moedertaal is, zullen ze mensen waarschijnlijk helpen het Debian-project te
begrijpen, en u kunt ze ook altijd naar uw eigen taal vertalen. Deze gedrukte
folders kunnen worden aangevraagd bij
<a href="mailto:meskes@debian.org">Michael Meskes</a>.
</p>

<toc-add-entry name="demos">Demosystemen</toc-add-entry>

<p>Natuurlijk is het vaak een heel goed idee om een systeem op te zetten waarop
Debian functioneert om te demonstreren tot wat het
Debian-besturingssysteem in staat is en hoe men het kan gebruiken. Om een
systeem op te zetten, hoeft u alleen maar een machine beschikbaar te hebben en
Debian erop te installeren.</p>

<p>Mocht het systeem niet bemand kunnen worden, zet er dan gewoon een mooie demo
op. Er zijn tal van demo's in het Debian-archief; Mesa- of OpenGL-demo's kunnen
behoorlijk flitsend zijn. Een screensaver waarin Debian-ontwikkelaars worden
vermeld, kan eenvoudig worden samengesteld door een tekstbestand te genereren
met alle leden van het project met behulp van het commando:
<em>grep-available -n -s Maintainer ''  | sort -u</em>, en daarna de
screensaver <q>FlipText</q>, <q>Fontglide</q>, <q>Noseguy</q>, <q>Phosphor</q>
of <q>Starwars</q> te gebruiken.
Een andere mogelijkheid is mensen te laten surfen op het Internet met een
Debian Desktop.</p>

<p>U kunt ook een <a
href="https://wiki.debian.org/DebianInstaller/BabelBox">BabelBox</a>-demo
opzetten, wat iets complexer maar indrukwekkender is. De Babelbox-demo,
ontwikkeld door Christian Perrier, is een systeem dat gebruikmaakt van de
automatiseringsmogelijkheden van het Debian-installatieprogramma om Debian
automatisch op een systeem te installeren in (facultatief) één na één alle
verschillende talen waarnaar het installatieprogramma is vertaald. Lees de
<a href="https://wiki.debian.org/DebianInstaller/BabelBox">wiki</a>-pagina voor
alle details over hoe u het systeem moet opzetten (u heeft twee verschillende
machines nodig).</p>

<p>Indien er behoefte is aan hardware, kunnen we de <a
href="https://wiki.debian.org/Teams/Events/DebianEventsBox">Debian
evenementenkoffer</a> naar u opsturen.

<toc-add-entry name="stickers">Stickers</toc-add-entry>

<p>Eén van de dingen die mensen het meest waarderen op een stand zijn stickers.
Debian-stickers zijn heel handig omdat ze kunnen gebruikt worden om de
afbeelding van uw favoriete besturingssysteem op laptops, monitors of servers
te plakken (Opmerking: stickers met een transparante achtergrond zijn veel
toffer dan deze met een witte achtergrond).</p>

<p>U kunt zowel de <a href="$(HOME)/logos/">logo's</a> (onder de vermelde
voorwaarden) als een van de beschikbare
<a href="materials/stickers/">stickers</a> gebruiken.
In de onderliggende map <a href="materials/stickers/pages/">pagina's</a> vindt u
de bron voor het maken van een pagina vol met logo's die u handig kunt
gebruiken om af te drukken met een normale (kleuren)printer op zelfklevend
papier.</p>

<p>Daarnaast heeft de <a href="https://www.fsf.org/">Free Software
Foundation</a> een aantal toffe stickers waarop staat: <em>GNU protected</em>,
<em>GNU &amp; Linux &ndash; the dynamic duo</em> en <em>GNU/Linux
inside</em>, die niemand mag missen, echt waar.</p>

<toc-add-entry name="foilstickers">Foliestickers</toc-add-entry>

<p>Stickers zijn geweldig om op een stand te gebruiken. Maar de meeste daarvan
zijn gewoon op papier gedrukt en worden vaak door zonlicht afgebroken, of
vernietigd door water of vuil. Een folie-etiket is een gelijmde folie die met
een speciaal soort plotter in zijn vorm wordt gesneden. Er is dus helemaal geen
sprake van een witte, transparante, of wat dan ook, achtergrond &ndash; alleen
maar de folie. Daarom zijn ze ideaal om aan te brengen op laptops, op auto's,
op voor het publiek bedoelde demonstratiecomputers in de stand, in het
algemeen op de stand zelf of op wat dan ook.</p>

<p>Een ander voordeel is de variabele schaal. De meest gebruikelijke maten
variëren van ongeveer 3&nbsp;cm tot 1,20&nbsp;m, afhankelijk van de grootte van
de plotter.</p>

<p>De stickers kunnen worden gebruikt om de stand er beter uit te laten zien; u
kunt ook kleinere stickers gebruiken om weg te geven aan bezoekers, enz.</p>

<p>Stickers zijn goedkoop te produceren, maar hun emotionele waarde is hoog,
zodat het heel goed is om ze te verkopen of om ze weg te geven in ruil voor
een donatie.</p>

<p>Op heel diverse Linux-conferenties en -tentoonstellingen, waaronder
Debconf - de conferentie van Debian, is gebleken dat bezoekers ze erg leuk
vinden en bereid zijn een donatie te geven.</p>

<p>In de <a href="materials/stickers/">map met stickers</a> kunt u aanvullende
informatie, voorbeelden en bestanden vinden. U kunt bijvoorbeeld één enkele
pagina op folie afdrukken met veel verschillende stickers. Hiervoor kunt u
gebruik maken van de <a href="materials/stickers/pages">afdruk op één enkele
pagina</a>.</p>

<toc-add-entry name="cards">Debian-visitekaartjes voor ontwikkelaars</toc-add-entry>

<p>Aangezien de mensen (ontwikkelaars of niet) in hun vrije tijd heel wat tijd
aan Debian besteden, is het leuk om een cadeautje voor ze klaar te maken. Een
Debian-visitekaartje is een leuk dingetje om te laten zien en vervult de mensen
die er een bezitten met een zekere trots!</p>

<p>Ook kunnen mensen die evenementen en stands organiseren en tijdens sociale
evenementen in contact komen met veel andere projectleiders en coördinatoren
hun visitekaartjes uitwisselen. Het uitdelen van visitekaartjes met het
Debian-logo draagt bij aan het imago van Debian en is handig voor het
overhandigen van belangrijke informatie die u normaal gesproken op een
papiertje moet krabbelen (zoals uw e-mailadres, uw GPG vingerafdruk...).</p>

<p>Er bestaat een <a href="materials/business-cards/">prototype</a> voor
visitekaartjes dat u kunt gebruiken. Dit aanpassen is eenvoudig, aangezien u
alleen het bestand <tt>card.tex</tt> met de persoonlijke gegevens hoeft te
bewerken en gewoon <tt>latex card.tex</tt> hoeft uit te voeren. Bekijk het
<a href="materials/business-cards/traditional/card.ps.gz">resultaat</a>.</p>


<toc-add-entry name="slides">Beeldpresentatie</toc-add-entry>

<p>Als u inderhaast een spreekbeurt moet houden over &ldquo;Wat is het Debian
Project?&rdquo;, maakt u zich dan geen zorgen. U kunt de beeldpresentatie
<a href="materials/slides/debian_mgp_slides.tar.gz">ophalen</a> of in de
bronnen  ervan zoeken naar ideeën. U kunt ook een kijkje nemen in de
<a href="https://wiki.debian.org/Presentations">lijst met voordrachten</a> die
door verschillende mensen zijn gegeven over Debian of over bepaalde aspecten
van het project. Joey heeft ook een algemeen
<a href="https://www.infodrom.org/Debian/events/talk-general.html">kader</a>
voor een voordracht in verband met Debian opgesteld, waar u uw eigen voordracht
misschien op kunt baseren.</p>

<p>Er zijn ook enkele mooie achtergrondafbeeldingen beschikbaar die u kunt
gebruiken om uw beeldpresentatie te verfraaien, bijvoorbeeld dit mooie
<a href="http://themes.freshmeat.net/projects/debian_gel_logo/">Debian
Gel Logo</a> van Patrick McFarland. Als u meer geschikte afbeeldingen kent,
<a href="mailto:events@debian.org">laat het ons dan weten</a>.
</p>


<toc-add-entry name="tshirts">T-shirts</toc-add-entry>

<p>T-shirts zijn vrij duur om te maken, maar zijn goede geschenken voor mensen
die helpen in de stand of bij de organisatie van het evenement (als ze echt zo
behulpzaam zijn). U kunt <a href="materials/tshirts/">wat grafisch materiaal</a>
bekijken dat voor t-shirts wordt gebruikt.</p>

<p>Als u van uw stand een verkooppunt van reclamegoederen wilt maken, kunt u
(als het evenement dat toelaat) T-shirts proberen te verkopen. Er staan een
aantal winkels op de <a href="$(HOME)/events/merchandise">pagina over
reclamegoederen</a> met wie u kunt samenwerken. Als u meer ideeën nodig hebt,
kunt u ernaar vragen op de <a href="eventsmailinglists">passende
evenementenmailinglist</a> of, als dat niet vruchtbaar bleek, op
<a href="mailto:events@debian.org">events@debian.org</a>.</p>


<toc-add-entry name="cdroms">cd's</toc-add-entry>

<p>Als u lege cd's koopt om cd's weg te geven, wil u misschien wat meer geld
uitgeven en wat cd's branden. Vergeet niet om te proberen ze er mooier te laten
uitzien dan een gewone lege cd (gebruik hiervoor illustratiemateriaal). De
belangrijkste bron van informatie over Debian cd's zijn de
<a href="$(HOME)/CD/">Debian cd-pagina's</a> die het proces van het downloaden
in detail beschrijven. Ze bevatten ook behoorlijk mooi
<a href="$(HOME)/CD/artwork/">illustratiemateriaal</a> dat u kunt gebruiken als
omslag en als label. Neem alle nodige maatregelen om
<a href="$(HOME)/CD/vendors/legal">in overeenstemming</a> te blijven met de
bijbehorende licentie van de software.</p>


<toc-add-entry name="other">Andere bronnen</toc-add-entry>

<p>Een goede bron van ideeën en beeldmateriaal welke u kunt gebruiken om
reclame te maken voor Debian is
<a href="http://gnuart.onshore.com/">http://gnuart.onshore.com/</a>.</p>

<p>Ander afdrukbaar advertentiemateriaal zoals de <q>Referentiekaart</q> of de
<q>Debian spiekkubus</q> is beschikbaar op de
<a href="https://wiki.debian.org/Promote/PrintAdvertising">wiki-pagina over
gedrukt advertentiemateriaal</a>.</p>

<p><Strong>Opmerking:</Strong> Sommige bronnen (zoals folders, brochures of
beeldpresentaties) zijn mogelijk verouderd sinds ze werden opgesteld. Als u ze
gebruikt voor een nieuw evenement, controleer dan of de informatie up-to-date
is en werk deze zo nodig bij. Als u dat doet, kunt u de wijzigingen doorgeven
door een bug te sturen naar debian-www of door een mail te sturen naar
<a href="mailto:events@debian.org">events@debian.org</a>.</p>


<toc-add-entry name="credits">Dankbetuiging</toc-add-entry>

<p>Het materiaal dat hier beschreven is en waarnaar verwezen werd, is de
bijdrage van veel mensen die hiervoor bedankt moeten worden, omdat dit het
leven van andere mensen gemakkelijker maakt. Dank gaat uit naar</p>

<ul>
<li>Anand Kumria voor de posters
    (<a href="https://web.archive.org/web/20080515224646/http://people.debian.org/~akumria/posters/">origineel hier</a>).</li>
<li>Frank Neumann voor de folders en de beeldpresentaties.</li>
<li>Javier Viñuales voor de stickers op één pagina.</li>
<li>Jason Gunthorpe voor de visitekaartjes.</li>
<li>Jim Westveer droeg bij aan het ontwerp van enkele t-shirt en cd-labels
    (<a href="https://web.archive.org/web/20080515224601/http://people.debian.org/~jwest/graphics/">origineel hier</a>).</li>
<li>Wichert Akkerman voor de folders en de beeldpresentaties.</li>
<li>Bdale Garbee voor de folders en de beeldpresentaties.</li>
<li>Thimo Neubauer voor de wereldkaart.</li>
<li>Dennis Stampfer voor de foliestickers.</li>
</ul>
